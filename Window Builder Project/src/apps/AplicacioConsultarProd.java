package apps;

import java.awt.EventQueue;

import tastat.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import java.awt.Dialog.ModalityType;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.ListIterator;

import javax.swing.ListSelectionModel;

public class AplicacioConsultarProd extends JDialog {

	private Magatzem prova;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTable tConsultarProducte;
	private JComboBox<String> cbProveidor;
	private JComboBox<String> cbNomProducte;
	private JComboBox<Integer> cbCodiProd;
	private JRadioButton rdbtnTots, rdbtnVendibles, rdbtnIngredients;
	private JCheckBox chckbxTrecamentStock;

	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AplicacioConsultarProd window = new AplicacioConsultarProd();
					window.frmConsultarProductes.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	public AplicacioConsultarProd(Magatzem magatzem) {
		prova = magatzem;
		Collections.sort(prova.getProductes());
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.setTitle("Consultar Productes");
		this.setResizable(false);
		this.setBounds(100, 100, 841, 500);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.getContentPane().setLayout(null);

		JLabel lblCodiProducte = new JLabel("Codi Producte:");
		lblCodiProducte.setBounds(10, 86, 97, 20);
		this.getContentPane().add(lblCodiProducte);

		JLabel lblNomProducte = new JLabel("Nom producte");
		lblNomProducte.setBounds(10, 145, 97, 20);
		this.getContentPane().add(lblNomProducte);

		JLabel lblProvedor = new JLabel("Proveïdor:");
		lblProvedor.setBounds(10, 117, 97, 20);
		this.getContentPane().add(lblProvedor);

		cbCodiProd = new JComboBox<Integer>();
		cbCodiProd.setBounds(117, 86, 86, 20);

		cbNomProducte = new JComboBox<String>();
		cbNomProducte.setBounds(117, 145, 115, 20);

		for (Producte p : prova.getProductes()) {
			cbCodiProd.addItem(p.getCodiProducte());
			cbNomProducte.addItem(p.getNomProducte());
		}

		cbNomProducte.addItem("Cap Filtre");
		cbCodiProd.addItem(null);
		this.getContentPane().add(cbNomProducte);
		this.getContentPane().add(cbCodiProd);

		cbProveidor = new JComboBox<String>();
		cbProveidor.setBounds(117, 117, 115, 20);
		for (Proveidor p : prova.getProveidors()) {
			cbProveidor.addItem(p.getNomProveidor());
		}
		cbProveidor.addItem("No té proveidor");
		cbProveidor.addItem("Cap Filtre");
		this.getContentPane().add(cbProveidor);

		cbCodiProd.setSelectedItem(null);
		cbNomProducte.setSelectedItem("Cap Filtre");
		cbProveidor.setSelectedItem("Cap Filtre");

		cbCodiProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cbCodiProd.getSelectedItem() != null) {
					Integer idProd = (Integer) cbCodiProd.getSelectedItem();
					String nomprod = null;
					for (Producte p : prova.getProductes()) {
						if (p.getCodiProducte() == idProd) {
							nomprod = p.getNomProducte();
							break;
						}
					}

					if (nomprod != null)
						cbNomProducte.setSelectedItem(nomprod);

				} else {
					cbNomProducte.setSelectedItem("Cap Filtre");
				}
			}
		});

		cbNomProducte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!cbNomProducte.getSelectedItem().equals("Cap Filtre")) {
					Integer idProd = null;
					String nomprod = (String) cbNomProducte.getSelectedItem();

					for (Producte p : prova.getProductes()) {
						if (p.getNomProducte().equals(nomprod)) {
							idProd = p.getCodiProducte();
							break;
						}
					}

					if (idProd != null)
						cbCodiProd.setSelectedItem(idProd);

				} else {
					cbCodiProd.setSelectedItem(null);
				}
			}
		});

		JPanel jpTipusProducte = new JPanel();
		jpTipusProducte.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Tipus Producte",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		jpTipusProducte.setBounds(242, 82, 121, 106);
		this.getContentPane().add(jpTipusProducte);
		jpTipusProducte.setLayout(null);

		rdbtnTots = new JRadioButton("Tots");
		rdbtnTots.setBounds(6, 16, 109, 23);
		jpTipusProducte.add(rdbtnTots);
		rdbtnTots.setSelected(true);
		buttonGroup.add(rdbtnTots);

		rdbtnVendibles = new JRadioButton("Vendibles");
		rdbtnVendibles.setBounds(6, 46, 109, 23);
		jpTipusProducte.add(rdbtnVendibles);
		buttonGroup.add(rdbtnVendibles);

		rdbtnIngredients = new JRadioButton("Ingredients");
		rdbtnIngredients.setBounds(6, 76, 109, 23);
		jpTipusProducte.add(rdbtnIngredients);
		buttonGroup.add(rdbtnIngredients);

		chckbxTrecamentStock = new JCheckBox("Solament Trencament Stock");
		chckbxTrecamentStock.setBounds(369, 85, 225, 23);
		this.getContentPane().add(chckbxTrecamentStock);

		JButton btnRefrescar = new JButton("Refrescar");
		btnRefrescar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mostrar();
			}
		});
		btnRefrescar.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnRefrescar.setBounds(600, 104, 149, 46);
		this.getContentPane().add(btnRefrescar);

		JButton buttonReset = new JButton("Reset Filtres");
		buttonReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				resetFiltres();
				resetTablas();
			}
		});
		buttonReset.setFont(new Font("Tahoma", Font.BOLD, 11));
		buttonReset.setBounds(373, 119, 149, 23);
		this.getContentPane().add(buttonReset);

		JScrollPane spConsultarProducte = new JScrollPane();
		spConsultarProducte.setBounds(10, 194, 805, 197);
		this.getContentPane().add(spConsultarProducte);

		tConsultarProducte = new JTable();
		tConsultarProducte.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tConsultarProducte.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Id Producte", "Nom Producte", "Preu Venda", "Stock Total", "Unitat Mesura", "Stock Min", "Tipus", "Proveidor"
			}
		) {
			Class[] columnTypes = new Class[] {
				Integer.class, String.class, Double.class, Integer.class, String.class, Integer.class, String.class, String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
			
			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false, false, false};

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tConsultarProducte.getColumnModel().getColumn(1).setPreferredWidth(142);
		tConsultarProducte.getColumnModel().getColumn(4).setPreferredWidth(89);
		spConsultarProducte.setViewportView(tConsultarProducte);

		JTextPane txtpnConsultarProductes = new JTextPane();
		txtpnConsultarProductes.setText("CONSULTAR PRODUCTES");
		txtpnConsultarProductes.setForeground(Color.DARK_GRAY);
		txtpnConsultarProductes.setFont(new Font("Arial Black", Font.BOLD, 32));
		txtpnConsultarProductes.setEditable(false);
		txtpnConsultarProductes.setBackground(SystemColor.inactiveCaption);
		txtpnConsultarProductes.setBounds(0, 0, 825, 78);
		this.getContentPane().add(txtpnConsultarProductes);
	}

	private void resetFiltres() {
		cbCodiProd.setSelectedItem(null);
		cbNomProducte.setSelectedItem("Cap Filtre");
		cbProveidor.setSelectedItem("Cap Filtre");
		rdbtnTots.setSelected(true);
		chckbxTrecamentStock.setSelected(false);
	}

	private void resetTablas() {
		tConsultarProducte.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Id Producte", "Nom Producte", "Preu Venda", "Stock Total", "Unitat Mesura", "Stock Min", "Tipus", "Proveidor"
				}
			) {
				Class[] columnTypes = new Class[] {
					Integer.class, String.class, Double.class, Integer.class, String.class, Integer.class, String.class, String.class
				};
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				
				boolean[] columnEditables = new boolean[] { false, false, false, false, false, false, false, false};

				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		tConsultarProducte.getColumnModel().getColumn(1).setPreferredWidth(142);
		tConsultarProducte.getColumnModel().getColumn(4).setPreferredWidth(89);
	}

	private void mostrar() {
		resetTablas();

		ArrayList<Producte> productesMostrar = new ArrayList<Producte>();

		if (cbCodiProd.getSelectedItem() != null) {
			int id = (int) cbCodiProd.getSelectedItem();
			for (Producte p : prova.getProductes()) {
				if (p.getCodiProducte() == id) {
					productesMostrar.add(p);
					break;
				}
			}
		}else if(!cbProveidor.getSelectedItem().equals("Cap Filtre")) {
			String proveidor = cbProveidor.getSelectedItem().toString();
			for(Producte pr: prova.getProductes()) {
				if(pr.getProveidor() != null) {
					if(pr.getProveidor().getNomProveidor().equals(proveidor)) {
						productesMostrar.add(pr);
					}
				}
			}
		} else {
			for(Producte p: prova.getProductes()) {
				productesMostrar.add(p);
			}
		}
		
		if(!rdbtnTots.isSelected()) {
			ListIterator<Producte> li = productesMostrar.listIterator();
			if(rdbtnIngredients.isSelected()) {
				while(li.hasNext()) {
					Producte p = li.next();
					if(!p.getTipus().toString().equals(Tipus.INGREDIENT.toString())) {
						li.remove();
					}
				}
			} else {
				while(li.hasNext()) {
					Producte p = li.next();
					if(!p.getTipus().toString().equals(Tipus.VENDIBLE.toString())) {
						li.remove();
					}
				}
			}
		}
		
		if(chckbxTrecamentStock.isSelected()) {
			ListIterator<Producte> li = productesMostrar.listIterator();
			while(li.hasNext()) {
				Producte p = li.next();
				if(p.getStock() >= p.getStockMinim()) {
					li.remove();
				}
			}
		}
		
		DefaultTableModel taulaProd = (DefaultTableModel) tConsultarProducte.getModel();

		if (taulaProd != null) {
			for (Producte p : productesMostrar) {
				int idProd = p.getCodiProducte();
				String nomProd = p.getNomProducte();
				double preu = p.getPreuVenda();
				int stock = p.getStock();
				String unitat = p.getUnitatMesura().toString();
				int stockMinim = p.getStockMinim();
				String tipus = p.getTipus().toString();
				String prov;
				if(p.getProveidor() != null)
					prov = p.getProveidor().getNomProveidor();
				else
					prov = "No té proveidor";

				taulaProd.addRow(new Object[] { idProd, nomProd, preu, stock, unitat, stockMinim, tipus, prov});
			}
		}

	}

	public Magatzem getMagatzem() {
		return prova;
	}
}
