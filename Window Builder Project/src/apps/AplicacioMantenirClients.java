package apps;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import java.awt.Dialog.ModalityType;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

import dialog.DialogAfegirModClient;
import dialog.DialogAfegirModProducte;
import tastat.*;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;

public class AplicacioMantenirClients extends JDialog {
	
	private Magatzem prova;
	private int countClients = 0;
	private int MAX;
	private int id = 0;
	private JTextField textIdClient;
	private JTextField textNomClient;
	private JTextField textCIF;
	private JTextField textActiu;
	private JTextField textDireccio;
	private JTextField textPoblacio;
	private JTextField textPais;
	private JTextField textContacte;
	private JTextField textTelefon;
	private JTextField textLatitud;
	private JTextField textLongitud;
	private JLabel lblError1, lblError2;


	/*public static void main(String[] args) {
		try {
			AplicacioMantenirClients dialog = new AplicacioMantenirClients();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public AplicacioMantenirClients(Magatzem magatzem) {
		prova = magatzem;
		MAX = prova.getClients().size();
		this.setResizable(false);
		this.setMinimumSize(new Dimension(570, 440));
		this.setTitle("Mantenir Clients");
		this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.getContentPane().setLayout(null);
		
		JTextPane txtpnMantenirClients = new JTextPane();
		txtpnMantenirClients.setText("MANTENIR CLIENTS");
		txtpnMantenirClients.setForeground(Color.DARK_GRAY);
		txtpnMantenirClients.setFont(new Font("Arial Black", Font.BOLD, 32));
		txtpnMantenirClients.setEditable(false);
		txtpnMantenirClients.setBackground(SystemColor.inactiveCaption);
		txtpnMantenirClients.setBounds(0, 0, 554, 82);
		getContentPane().add(txtpnMantenirClients);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBackground(Color.WHITE);
		panel.setBounds(10, 89, 534, 300);
		getContentPane().add(panel);
		
		JTextPane txtpnIdClient = new JTextPane();
		txtpnIdClient.setText("id Client:");
		txtpnIdClient.setEditable(false);
		txtpnIdClient.setBackground(Color.WHITE);
		txtpnIdClient.setBounds(41, 11, 95, 20);
		panel.add(txtpnIdClient);
		
		JTextPane txtpnNomClient = new JTextPane();
		txtpnNomClient.setText("Nom Client:");
		txtpnNomClient.setEditable(false);
		txtpnNomClient.setBackground(Color.WHITE);
		txtpnNomClient.setBounds(41, 36, 95, 20);
		panel.add(txtpnNomClient);
		
		JTextPane txtpnCif = new JTextPane();
		txtpnCif.setText("CIF:");
		txtpnCif.setEditable(false);
		txtpnCif.setBackground(Color.WHITE);
		txtpnCif.setBounds(41, 61, 95, 20);
		panel.add(txtpnCif);
		
		JTextPane txtpnActiu = new JTextPane();
		txtpnActiu.setText("Actiu:");
		txtpnActiu.setEditable(false);
		txtpnActiu.setBackground(Color.WHITE);
		txtpnActiu.setBounds(41, 86, 95, 20);
		panel.add(txtpnActiu);
		
		JTextPane txtpnDireccio = new JTextPane();
		txtpnDireccio.setText("Direcció:");
		txtpnDireccio.setEditable(false);
		txtpnDireccio.setBackground(Color.WHITE);
		txtpnDireccio.setBounds(41, 111, 95, 20);
		panel.add(txtpnDireccio);
		
		JTextPane txtpnPoblacio = new JTextPane();
		txtpnPoblacio.setText("Poblacio:");
		txtpnPoblacio.setEditable(false);
		txtpnPoblacio.setBackground(Color.WHITE);
		txtpnPoblacio.setBounds(41, 136, 95, 20);
		panel.add(txtpnPoblacio);
		
		JTextPane txtpnPais = new JTextPane();
		txtpnPais.setText("Pais:");
		txtpnPais.setEditable(false);
		txtpnPais.setBackground(Color.WHITE);
		txtpnPais.setBounds(292, 11, 95, 20);
		panel.add(txtpnPais);
		
		JTextPane txtpnPersonaContacte = new JTextPane();
		txtpnPersonaContacte.setText("Contacte:");
		txtpnPersonaContacte.setEditable(false);
		txtpnPersonaContacte.setBackground(Color.WHITE);
		txtpnPersonaContacte.setBounds(292, 36, 95, 20);
		panel.add(txtpnPersonaContacte);
		
		JTextPane txtpnTelefon = new JTextPane();
		txtpnTelefon.setText("Telefon:");
		txtpnTelefon.setEditable(false);
		txtpnTelefon.setBackground(Color.WHITE);
		txtpnTelefon.setBounds(292, 61, 95, 20);
		panel.add(txtpnTelefon);
		
		JTextPane txtpnLatitud = new JTextPane();
		txtpnLatitud.setText("Latitud:");
		txtpnLatitud.setEditable(false);
		txtpnLatitud.setBackground(Color.WHITE);
		txtpnLatitud.setBounds(292, 86, 95, 20);
		panel.add(txtpnLatitud);
		
		JTextPane txtpnLongitud = new JTextPane();
		txtpnLongitud.setText("Longitud:");
		txtpnLongitud.setEditable(false);
		txtpnLongitud.setBackground(Color.WHITE);
		txtpnLongitud.setBounds(292, 111, 95, 20);
		panel.add(txtpnLongitud);
		
		lblError1 = new JLabel("");
		lblError1.setHorizontalAlignment(SwingConstants.CENTER);
		lblError1.setForeground(Color.RED);
		lblError1.setBounds(136, 263, 250, 14);
		panel.add(lblError1);
		
		lblError2 = new JLabel("");
		lblError2.setHorizontalAlignment(SwingConstants.CENTER);
		lblError2.setForeground(Color.RED);
		lblError2.setBounds(136, 279, 250, 15);
		panel.add(lblError2);
		
		textIdClient = new JTextField();
		textIdClient.setColumns(10);
		textIdClient.setAlignmentX(1.0f);
		textIdClient.setBounds(141, 11, 95, 20);
		panel.add(textIdClient);
		
		textNomClient = new JTextField();
		textNomClient.setEditable(false);
		textNomClient.setColumns(10);
		textNomClient.setAlignmentX(1.0f);
		textNomClient.setBounds(141, 36, 132, 20);
		panel.add(textNomClient);
		
		textCIF = new JTextField();
		textCIF.setEditable(false);
		textCIF.setColumns(10);
		textCIF.setAlignmentX(1.0f);
		textCIF.setBounds(141, 61, 95, 20);
		panel.add(textCIF);
		
		textActiu = new JTextField();
		textActiu.setEditable(false);
		textActiu.setColumns(10);
		textActiu.setAlignmentX(1.0f);
		textActiu.setBounds(141, 86, 95, 20);
		panel.add(textActiu);
		
		textDireccio = new JTextField();
		textDireccio.setEditable(false);
		textDireccio.setColumns(10);
		textDireccio.setAlignmentX(1.0f);
		textDireccio.setBounds(141, 111, 132, 20);
		panel.add(textDireccio);
		
		textPoblacio = new JTextField();
		textPoblacio.setEditable(false);
		textPoblacio.setColumns(10);
		textPoblacio.setAlignmentX(1.0f);
		textPoblacio.setBounds(141, 136, 95, 20);
		panel.add(textPoblacio);
		
		textPais = new JTextField();
		textPais.setEditable(false);
		textPais.setColumns(10);
		textPais.setAlignmentX(1.0f);
		textPais.setBounds(392, 11, 95, 20);
		panel.add(textPais);
		
		textContacte = new JTextField();
		textContacte.setEditable(false);
		textContacte.setColumns(10);
		textContacte.setAlignmentX(1.0f);
		textContacte.setBounds(392, 36, 95, 20);
		panel.add(textContacte);
		
		textTelefon = new JTextField();
		textTelefon.setEditable(false);
		textTelefon.setColumns(10);
		textTelefon.setAlignmentX(1.0f);
		textTelefon.setBounds(392, 61, 95, 20);
		panel.add(textTelefon);
		
		textLatitud = new JTextField();
		textLatitud.setEditable(false);
		textLatitud.setColumns(10);
		textLatitud.setAlignmentX(1.0f);
		textLatitud.setBounds(392, 86, 95, 20);
		panel.add(textLatitud);
		
		textLongitud = new JTextField();
		textLongitud.setEditable(false);
		textLongitud.setColumns(10);
		textLongitud.setAlignmentX(1.0f);
		textLongitud.setBounds(392, 111, 95, 20);
		panel.add(textLongitud);
		
		JButton buttonBuscar = new JButton("BUSCAR");
		buttonBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscarClient();
			}
		});
		buttonBuscar.setFont(new Font("Tahoma", Font.BOLD, 9));
		buttonBuscar.setBounds(212, 179, 89, 23);
		panel.add(buttonBuscar);
		
		JButton buttonAnterior = new JButton("<");
		buttonAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				previousClient();
			}
		});
		buttonAnterior.setBounds(159, 179, 50, 23);
		panel.add(buttonAnterior);
		
		JButton buttonSeguent = new JButton(">");
		buttonSeguent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextClient();
			}
		});
		buttonSeguent.setBounds(304, 179, 50, 23);
		panel.add(buttonSeguent);
		
		JButton buttonAfegir = new JButton("AFEGIR");
		buttonAfegir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirClient();
			}
		});
		buttonAfegir.setFont(new Font("Tahoma", Font.BOLD, 9));
		buttonAfegir.setBounds(117, 206, 87, 23);
		panel.add(buttonAfegir);
		
		JButton buttonAltaBaixa = new JButton("ALTA/BAIXA");
		buttonAltaBaixa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				donarDeBaixa();
			}
		});
		buttonAltaBaixa.setFont(new Font("Tahoma", Font.BOLD, 9));
		buttonAltaBaixa.setBounds(208, 206, 101, 23);
		panel.add(buttonAltaBaixa);
		
		JButton buttonModificar = new JButton("MODIFICAR");
		buttonModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modificarClient();
			}
		});
		buttonModificar.setFont(new Font("Tahoma", Font.BOLD, 9));
		buttonModificar.setBounds(314, 206, 102, 23);
		panel.add(buttonModificar);
		
	}

	private void modificarClient() {
		Client client = trobarClient(id);
		if(client != null) {
			JDialog dialogPanel = new DialogAfegirModClient(prova, id);
			dialogPanel.setLocationRelativeTo(null);
			dialogPanel.setVisible(true);
		
			if (((DialogAfegirModClient) dialogPanel).isCorrecte()) {
				String nomClient = ((DialogAfegirModClient) dialogPanel).getTextNomClient().getText();
				String CIF = ((DialogAfegirModClient) dialogPanel).getTextCIF().getText();
				String direccio = ((DialogAfegirModClient) dialogPanel).getTextDireccio().getText();
				String poblacio = ((DialogAfegirModClient) dialogPanel).getTextPoblacio().getText();
				String pais = ((DialogAfegirModClient) dialogPanel).getTextPais().getText();
				String personaContacte = ((DialogAfegirModClient) dialogPanel).getTextContacte().getText();
				String telefon = ((DialogAfegirModClient) dialogPanel).getTextTelefon().getText();
				double latitud = (double) ((DialogAfegirModClient) dialogPanel).getSpinnerLatitud().getValue();
				double longitud = (double) ((DialogAfegirModClient) dialogPanel).getSpinnerLongitud().getValue();
				
				Client c = trobarClient(id);
				
				c.setNomClient(nomClient);
				c.setCIF(CIF);
				c.setDireccio(direccio);
				c.setPoblacio(poblacio);
				c.setPoblacio(poblacio);
				c.setPais(pais);
				c.setPersonaContacte(personaContacte);
				c.setTelefon(telefon);
				c.setLatitud(latitud);
				c.setLongitud(longitud);
				
				int contador = 0;
				mostrar(c);
				for (Client cl: prova.getClients()) {
					contador++;
					if (c.getIdClient() == cl.getIdClient()) {
						break;
					}
				}
				countClients = contador;
				id = c.getIdClient();
				
				
			} else {
				lblError1.setText("No s'ha afegit el Client!");
				lblError1.setText("Faltaven camps per omplir o s'ha cancelat...");
			}
			
			dialogPanel.dispose();
		} else {
			lblError1.setText("No hi ha cap Client");
			lblError1.setText("seleccionat ara mateix...");
		}
	}

	private void afegirClient() {
		int idClient = Generador.getNextClient();
		JDialog dialogPanel = new DialogAfegirModClient(prova, idClient);
		dialogPanel.setLocationRelativeTo(null);
		dialogPanel.setVisible(true);
		
		if (((DialogAfegirModClient) dialogPanel).isCorrecte()) {
			String nomClient = ((DialogAfegirModClient) dialogPanel).getTextNomClient().getText();
			String CIF = ((DialogAfegirModClient) dialogPanel).getTextCIF().getText();
			String direccio = ((DialogAfegirModClient) dialogPanel).getTextDireccio().getText();
			String poblacio = ((DialogAfegirModClient) dialogPanel).getTextPoblacio().getText();
			String pais = ((DialogAfegirModClient) dialogPanel).getTextPais().getText();
			String personaContacte = ((DialogAfegirModClient) dialogPanel).getTextContacte().getText();
			String telefon = ((DialogAfegirModClient) dialogPanel).getTextTelefon().getText();
			double latitud = (double) ((DialogAfegirModClient) dialogPanel).getSpinnerLatitud().getValue();
			double longitud = (double) ((DialogAfegirModClient) dialogPanel).getSpinnerLongitud().getValue();
			
			
			Client c = new Client(idClient, nomClient, CIF, false, direccio, poblacio, pais, personaContacte, telefon, latitud, longitud);
			prova.getClients().add(c);
			MAX = prova.getClients().size();
			countClients = MAX;
			mostrar(c);
			id = c.getIdClient();
		} else {
			Generador.setClientActual((Generador.getClientActual() - 1));
			lblError1.setText("No s'ha afegit el Client!");
			lblError1.setText("Faltaven camps per omplir o s'ha cancelat...");
		}
		
		dialogPanel.dispose();
	}

	private void donarDeBaixa() {
		Client c = trobarClient(id);
		if(c != null) {
			if(c.isActiu()) {
				c.setActiu(false);
			} else {
				c.setActiu(true);
			}
			
			mostrar(c);
			
		} else {
			lblError1.setText("No hi ha un Client seleccionat");
		}
	}

	private void nextClient() {
		Client c = null;

		if ((countClients + 1) <= MAX) {
			do {
				id++;
				c = trobarClient(id);
			} while (c == null);
			countClients++;
			mostrar(c);
		} else if (MAX == 0) {
			lblError1.setText("No hi ha Clients");
		}
		
	}

	private void buscarClient() {
		Client c = null;

		if (!textIdClient.getText().equals("") && textIdClient.getText().matches("^[0-9]+$")) {
			id = Integer.parseInt(textIdClient.getText());
			c = trobarClient(id);
			if (c == null) {
				lblError1.setText("No existeix cap Client");
				lblError2.setText("amb ID Client " + id);
				id = 0;
				countClients = 0;
			} else {
				int contador = 0;
				mostrar(c);
				for (Client client : prova.getClients()) {
					contador++;
					if (c.getIdClient() == client.getIdClient()) {
						break;
					}
				}
				countClients = contador;
			}
		} else {
			if (textIdClient.getText().equals("")) {
				lblError1.setText("Introdueix una ID o");
				lblError2.setText("navega amb els botons < o >");
			} else {
				lblError1.setText("Només es poden");
				lblError2.setText("introduir numeros!");
			}
		}
	}

	private void previousClient() {
		Client c = null;

		if ((countClients - 1) > 0) {
			do {
				id--;
				c = trobarClient(id);
				countClients--;
			} while (c == null);
			c = trobarClient(id);
			mostrar(c);
		} else if (countClients == 0 && MAX != 0) {
			id = prova.getClients().get(0).getIdClient();
			countClients++;
			c = trobarClient(id);;
			mostrar(c);
		} else if (MAX == 0) {
			lblError1.setText("No hi ha Clients");
		}
		
	}
	
	private Client trobarClient(int id) {
		for(Client c: prova.getClients()) {
			if(c.getIdClient() == id) {
				return c;
			}
		}
		return null;
	}
	
	private void mostrar(Client c) {
		textIdClient.setText(Integer.toString(c.getIdClient()));
		textNomClient.setText(c.getNomClient());
		textCIF.setText(c.getCIF());
		textActiu.setText(Boolean.toString(c.isActiu()));
		textDireccio.setText(c.getDireccio());
		textPoblacio.setText(c.getPoblacio());
		textPais.setText(c.getPais());
		textContacte.setText(c.getPersonaContacte());
		textTelefon.setText(c.getTelefon());
		textLatitud.setText(Double.toString(c.getLatitud()));
		textLongitud.setText(Double.toString(c.getLongitud()));
		
		
		
	}
	
	public Magatzem getMagatzem() {
		return prova;
	}
}
