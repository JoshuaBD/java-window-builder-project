package apps;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JRadioButton;
import javax.swing.UIManager;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import dialog.DialogAfegirComanda;
import dialog.DialogAfegirModLiniesComanda;
import tastat.*;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.JTextPane;

public class AplicacioMantenirComandes extends JDialog {

	private Magatzem prova;
	private int id = 0;
	private int MAX;
	private int countCom = 0;
	private JTextField tfComanda;
	private JTextField tfClient;
	private JTextField tfDataComanda;
	private JTextField tfDataL;
	private JTextField tfPorts;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTable tbComandes;
	private JTextField tfValorTotal;
	private JTextField tfC;
	private JLabel lblError;
	private JRadioButton rdbtnPendent = new JRadioButton("Pendent");
	private JRadioButton rdbtnPreparada = new JRadioButton("Preparada");
	private JRadioButton rdbtnTransport = new JRadioButton("Transport");
	private JRadioButton rdbtnLliurada = new JRadioButton("Lliurada");

	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AplicacioMantenirComandes window = new AplicacioMantenirComandes();
					window.frmMantenirComanda.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	public AplicacioMantenirComandes(Magatzem magatzem) {
		setResizable(false);
		prova = magatzem;
		MAX = prova.getComandes().size();
		this.setTitle("Mantenir Comanda");
		this.setBounds(100, 100, 530, 440);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.getContentPane().setLayout(null);

		JLabel lblComanda = new JLabel("Comanda");
		lblComanda.setBounds(10, 97, 80, 14);
		this.getContentPane().add(lblComanda);

		tfComanda = new JTextField();
		tfComanda.setEditable(false);
		tfComanda.setBounds(87, 97, 86, 20);
		this.getContentPane().add(tfComanda);
		tfComanda.setColumns(10);

		JPanel panel = new JPanel();
		panel.setForeground(Color.LIGHT_GRAY);
		panel.setBorder(new LineBorder(Color.LIGHT_GRAY, 5));
		panel.setBounds(10, 118, 376, 107);
		this.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblClient = new JLabel("Client");
		lblClient.setBounds(6, 19, 42, 14);
		panel.add(lblClient);

		tfClient = new JTextField();
		tfClient.setEditable(false);
		tfClient.setBounds(186, 16, 35, 20);
		panel.add(tfClient);
		tfClient.setColumns(10);

		JLabel lblData = new JLabel("Data comanda");
		lblData.setBounds(6, 40, 86, 14);
		panel.add(lblData);

		tfDataComanda = new JTextField();
		tfDataComanda.setEditable(false);
		tfDataComanda.setBounds(87, 37, 71, 20);
		panel.add(tfDataComanda);
		tfDataComanda.setColumns(10);

		JLabel lblDataLliurament = new JLabel("Data lliurament");
		lblDataLliurament.setBounds(175, 40, 86, 14);
		panel.add(lblDataLliurament);

		tfDataL = new JTextField();
		tfDataL.setEditable(false);
		tfDataL.setBounds(254, 37, 71, 20);
		panel.add(tfDataL);
		tfDataL.setColumns(10);

		JLabel lblPorts = new JLabel("Ports:");
		lblPorts.setBounds(175, 68, 46, 14);
		panel.add(lblPorts);

		tfPorts = new JTextField();
		tfPorts.setEditable(false);
		tfPorts.setBounds(254, 65, 86, 20);
		panel.add(tfPorts);
		tfPorts.setColumns(10);

		tfC = new JTextField();
		tfC.setEditable(false);
		tfC.setBounds(58, 16, 100, 20);
		panel.add(tfC);
		tfC.setColumns(10);

		JPanel pEstatComanda = new JPanel();
		pEstatComanda.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Estat Comanda",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		pEstatComanda.setBounds(390, 104, 121, 121);
		this.getContentPane().add(pEstatComanda);
		pEstatComanda.setLayout(null);

		rdbtnPendent.setSelected(true);
		buttonGroup.add(rdbtnPendent);
		rdbtnPendent.setBounds(6, 16, 109, 23);
		pEstatComanda.add(rdbtnPendent);

		buttonGroup.add(rdbtnPreparada);
		rdbtnPreparada.setBounds(6, 41, 109, 23);
		pEstatComanda.add(rdbtnPreparada);

		buttonGroup.add(rdbtnTransport);
		rdbtnTransport.setBounds(6, 66, 109, 23);
		pEstatComanda.add(rdbtnTransport);

		buttonGroup.add(rdbtnLliurada);
		rdbtnLliurada.setBounds(6, 91, 109, 23);
		pEstatComanda.add(rdbtnLliurada);

		JPanel pComanda = new JPanel();
		pComanda.setBorder(new LineBorder(Color.LIGHT_GRAY));
		pComanda.setBounds(31, 227, 478, 140);
		this.getContentPane().add(pComanda);
		pComanda.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(6, 11, 472, 75);
		pComanda.add(scrollPane);

		tbComandes = new JTable();
		tbComandes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tbComandes.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Linea", "IdProducte", "Nom Producte", "Quantitat", "Preu Venda", "Import"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		scrollPane.setViewportView(tbComandes);
		JLabel lblImportTotal = new JLabel("Import Total : ");
		lblImportTotal.setBounds(238, 118, 97, 20);
		pComanda.add(lblImportTotal);
		lblImportTotal.setFont(new Font("Tahoma", Font.PLAIN, 13));

		tfValorTotal = new JTextField();
		tfValorTotal.setEditable(false);
		tfValorTotal.setBounds(338, 119, 140, 21);
		pComanda.add(tfValorTotal);
		tfValorTotal.setFont(new Font("Tahoma", Font.BOLD, 11));
		tfValorTotal.setBackground(Color.YELLOW);
		tfValorTotal.setForeground(Color.BLACK);
		tfValorTotal.setColumns(10);

		lblError = new JLabel("");
		lblError.setOpaque(true);
		lblError.setForeground(new Color(255, 0, 0));
		lblError.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblError.setBounds(10, 119, 228, 20);
		pComanda.add(lblError);

		JButton btnAfegirmodificarLinies = new JButton("Afegir/Modificar Linies");
		btnAfegirmodificarLinies.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				afegirModLinia();
			}
		});
		btnAfegirmodificarLinies.setHorizontalAlignment(SwingConstants.LEFT);
		btnAfegirmodificarLinies.setForeground(Color.BLACK);
		btnAfegirmodificarLinies.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAfegirmodificarLinies.setBackground(Color.WHITE);
		btnAfegirmodificarLinies.setBounds(6, 97, 140, 23);
		pComanda.add(btnAfegirmodificarLinies);
		
		JButton buttonEsbrorrarLinia = new JButton("Esborrar Linia");
		buttonEsbrorrarLinia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = tbComandes.getSelectedRow();
				DefaultTableModel model = (DefaultTableModel) tbComandes.getModel();

				if (i != -1) {
					int id = (int) model.getValueAt(i, 0);
					
					Comanda c = trobarComanda();
					c.getLinies().remove(id-1);

					model.removeRow(i);
					mostrar(c);
				}
			}
		});
		buttonEsbrorrarLinia.setHorizontalAlignment(SwingConstants.LEFT);
		buttonEsbrorrarLinia.setFont(new Font("Tahoma", Font.PLAIN, 9));
		buttonEsbrorrarLinia.setBackground(Color.WHITE);
		buttonEsbrorrarLinia.setBounds(156, 97, 97, 23);
		pComanda.add(buttonEsbrorrarLinia);

		JButton btnNovaComanda = new JButton("Nova Comanda");
		btnNovaComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				afegirComanda();
			}
		});
		btnNovaComanda.setForeground(new Color(0, 0, 0));
		btnNovaComanda.setBackground(Color.WHITE);
		btnNovaComanda.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnNovaComanda.setHorizontalAlignment(SwingConstants.LEFT);
		btnNovaComanda.setBounds(10, 374, 111, 23);
		this.getContentPane().add(btnNovaComanda);

		JButton btnEsborrarComanda = new JButton("Esborrar Comanda");
		btnEsborrarComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Comanda c = trobarComanda();
				if (c != null) {
					prova.getComandes().remove(c);
					MAX = prova.getComandes().size();
					id = prova.getComandes().get(0).getIdComanda();
					countCom = 1;

					lblError.setText("S'ha eliminat la comanda");

				} else {
					lblError.setText("No hi ha una Comanda seleccionada");
				}
			}
		});

		btnEsborrarComanda.setBackground(Color.WHITE);
		btnEsborrarComanda.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnEsborrarComanda.setHorizontalAlignment(SwingConstants.LEFT);
		btnEsborrarComanda.setBounds(131, 374, 121, 23);
		this.getContentPane().add(btnEsborrarComanda);
		// BUSQUEDA PER ID
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setVisible(false);
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				try {
					id = Integer.parseInt((tfComanda.getText()));
					Comanda c = trobarComanda();
					if (c == null) {
						lblError.setText("id no existeix");
						countCom = 0;
						id = 0;
					} else {
						int contador = 0;
						mostrar(c);
						for (Comanda com : prova.getComandes()) {
							contador++;
							if (com.getIdComanda() == c.getIdComanda())
								break;
						}
						countCom = contador;
					}

				} catch (NumberFormatException e) {
					lblError.setText("Ha de ser un numero");
				}
			}
		});
		btnBuscar.setBounds(183, 93, 89, 23);
		this.getContentPane().add(btnBuscar);
		// --------------------EDITAR COMANDA. HABILITEM ELS CAMPS:

		JButton EditarComanda = new JButton("Editar Comanda");
		EditarComanda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnBuscar.setVisible(false);
				if (rdbtnPendent.isSelected()) {
					lblError.setText("");
					btnBuscar.setVisible(true);
					activarEditable();
					comobox();
					comoboxClients();
				} else {
					desactivarEditable();
					btnBuscar.setVisible(false);
					lblError.setText("Ha de ser Estat pendent per editar");
				}

			}
		});
		EditarComanda.setBackground(Color.WHITE);
		EditarComanda.setFont(new Font("Tahoma", Font.PLAIN, 9));
		EditarComanda.setHorizontalAlignment(SwingConstants.LEFT);
		EditarComanda.setBounds(262, 374, 111, 23);
		this.getContentPane().add(EditarComanda);

		JButton btnUnaEsquerre = new JButton("<");
		btnUnaEsquerre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblError.setText("");
				btnBuscar.setVisible(false);
				Comanda c = null;

				if ((countCom - 1) > 0) {
					desactivarEditable();
					do {
						id--;
						c = trobarComanda();
						countCom--;
					} while (c == null);

					c = trobarComanda();
					mostrar(c);
				} else if (countCom == 0 && MAX != 0) {
					desactivarEditable();
					id = prova.getComandes().get(0).getIdComanda();
					countCom++;
					c = trobarComanda();
					mostrar(c);
				}
			}
		});
		btnUnaEsquerre.setFont(new Font("Tahoma", Font.PLAIN, 7));
		btnUnaEsquerre.setBounds(391, 374, 58, 23);
		this.getContentPane().add(btnUnaEsquerre);

		// Mostrar la primera comanda:

		JButton btnUnaDreta = new JButton(">");
		btnUnaDreta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lblError.setText("");
				btnBuscar.setVisible(false);
				Comanda c = null;

				if ((countCom + 1) <= MAX) {
					desactivarEditable();
					do {
						id++;
						c = trobarComanda();
					} while (c == null);

					countCom++;
					mostrar(c);
				}
			}
		});
		btnUnaDreta.setFont(new Font("Tahoma", Font.PLAIN, 7));
		btnUnaDreta.setBounds(453, 374, 58, 23);
		this.getContentPane().add(btnUnaDreta);
		
		JTextPane txtpnMantenirComandes = new JTextPane();
		txtpnMantenirComandes.setText("MANTENIR COMANDES");
		txtpnMantenirComandes.setForeground(Color.DARK_GRAY);
		txtpnMantenirComandes.setFont(new Font("Arial Black", Font.BOLD, 32));
		txtpnMantenirComandes.setEditable(false);
		txtpnMantenirComandes.setBackground(SystemColor.inactiveCaption);
		txtpnMantenirComandes.setBounds(0, 0, 524, 82);
		getContentPane().add(txtpnMantenirComandes);

	}

	public void afegirModLinia() {
		if (trobarComanda() != null) {

			Comanda comanda = trobarComanda();
			JDialog dialogPanel = new DialogAfegirModLiniesComanda(prova, comanda);
			dialogPanel.setLocationRelativeTo(null);
			dialogPanel.setVisible(true);

			if (((DialogAfegirModLiniesComanda) dialogPanel).isCorrecte()) {
					Producte p = null;
					int idprod = (int) ((DialogAfegirModLiniesComanda) dialogPanel).getComboIdProducte().getSelectedItem();
					for(Producte prod: prova.getProductes()) {
						if(prod.getCodiProducte() ==  idprod){
							p = prod;
							break;
						}
					}
					int q = (int) ((DialogAfegirModLiniesComanda) dialogPanel).getSpinnerQuantitat().getValue();
					double preu = (double) ((DialogAfegirModLiniesComanda) dialogPanel).getSpinnerPreuVenda().getValue();
					
					if(((DialogAfegirModLiniesComanda) dialogPanel).isModificar()) {
						int liniaC = (int) ((DialogAfegirModLiniesComanda) dialogPanel).getComboIdLinia().getSelectedItem();
						ComandaLinia cl = comanda.getLinies().get(liniaC-1);
						
						cl.setProducte(p);
						cl.setPreuVenda(preu);
						cl.setQuantitat(q);
						
					} else {
						ComandaLinia liniaNova = new ComandaLinia(p, q, preu);
						comanda.setLinies(new ArrayList<ComandaLinia>());
						comanda.getLinies().add(liniaNova);
					}
				}
				mostrar(comanda);

		} else {
			lblError.setText("No hi ha cap comanda seleccionada...");
		}
	}

	protected void afegirComanda() {
		int idComanda = Generador.getNextComanda();
		JDialog dialogPanel = new DialogAfegirComanda(prova, idComanda);
		dialogPanel.setLocationRelativeTo(null);
		dialogPanel.setVisible(true);

		if (((DialogAfegirComanda) dialogPanel).isCorrecte()) {
			int idClient = (int) ((DialogAfegirComanda) dialogPanel).getComboIdClient().getSelectedItem();
			Client client = null;
			for (Client c : prova.getClients()) {
				if (c.getIdClient() == idClient) {
					client = c;
					break;
				}

			}

			Date dataLliurament = ((DialogAfegirComanda) dialogPanel).getDataLliurament();
			Date dataComanda = new Date();
			ComandaEstat estat = null;
			String cEstat = ((DialogAfegirComanda) dialogPanel).getComboEstat().getSelectedItem().toString();

			if (cEstat.equals("LLIURADA"))
				estat = ComandaEstat.LLIURADA;
			else if (cEstat.equals("PENDENT"))
				estat = ComandaEstat.PENDENT;
			else if (cEstat.equals("PREPARADA"))
				estat = ComandaEstat.PREPARADA;
			else if (cEstat.equals("TRANSPORT"))
				estat = ComandaEstat.TRANSPORT;

			double portes = (double) ((DialogAfegirComanda) dialogPanel).getSpinnerPorts().getValue();

			Comanda c = new Comanda(idComanda, client, dataComanda, dataLliurament, estat, portes, null);

			prova.getComandes().add(c);
			MAX = prova.getComandes().size();
			countCom = MAX;
			mostrar(c);
			id = idComanda;

		} else {
			Generador.setComandaActual(Generador.getComandaActual() - 1);
		}

		dialogPanel.dispose();

	}

	public Comanda trobarComandaPerId(int idc) {
		for (Comanda c : prova.getComandes()) {
			if (idc == c.getIdComanda()) {
				return c;
			}
		}
		return null;
	}

	public Comanda trobarComanda() {
		for (Comanda c : prova.getComandes()) {
			if (id == c.getIdComanda()) {
				return c;
			}
		}
		return null;

	}

	public void mostrar(Comanda com) {

		tfComanda.setText(Integer.toString(com.getIdComanda()));
		tfC.setText(com.getClient().getNomClient());
		tfClient.setText(Integer.toString(com.getClient().getIdClient()));
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String fechaComoCadena = sdf.format(com.getDataComanda());
		tfDataComanda.setText(fechaComoCadena);
		String fechaComoCadena2 = sdf.format(com.getDataLliurament());
		tfDataL.setText(fechaComoCadena2);
		tfPorts.setText(Double.toString(com.getPortes()));
		// RADIO BUTTONS DEPENENT DE LA COMANDA EN AQUEST CAS TOTES SON pendent he hagut
		// de modificarles
		if (com.getEstat().equals(ComandaEstat.PENDENT)) {
			rdbtnPendent.setSelected(true);
		} else if (com.getEstat().equals(ComandaEstat.PREPARADA)) {
			rdbtnPreparada.setSelected(true);
		} else if (com.getEstat().equals(ComandaEstat.TRANSPORT)) {
			rdbtnTransport.setSelected(true);
		} else {
			rdbtnLliurada.setSelected(true);
		}

		/* TABLES: */
		List<ComandaLinia> lineas = com.getLinies();
		// RESET TABLAS

		tbComandes.setModel(new DefaultTableModel(
				new Object[][] {
				},
				new String[] {
					"Linea", "IdProducte", "Nom Producte", "Quantitat", "Preu Venda", "Import"
				}
			) {
				boolean[] columnEditables = new boolean[] {
					false, false, false, false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});

		DefaultTableModel taulaComanda = (DefaultTableModel) tbComandes.getModel();
		// tbComandes
		int q = 1;
		if (lineas != null) {
			for (ComandaLinia c : lineas) {
				int idProducte = c.getProducte().getCodiProducte();
				String nomProducte = c.getProducte().getNomProducte();
				int quantitat = c.getQuantitat();
				double preu = c.getPreuVenda();
				double imp = quantitat * preu;

				taulaComanda.addRow(new Object[] { q, idProducte, nomProducte, quantitat, preu, imp });
				q++;

			}
			double total = 0;
			for (int i = 0; i < tbComandes.getRowCount(); i++) {
				total = total + (Double) tbComandes.getValueAt(i, 5); // La columna 3 es la de total
			}
			tfValorTotal.setText(Double.toString(total) + " €");
		} else {
			tfValorTotal.setText("0");
		}

	}

	public void activarEditable() {
		tfComanda.setEditable(true);
		tfC.setEditable(true);
		tfClient.setEditable(true);
		tfDataComanda.setEditable(true);
		tfDataL.setEditable(true);
		tfPorts.setEditable(true);
	}

	public void desactivarEditable() {
		tfComanda.setEditable(false);
		tfC.setEditable(false);
		tfClient.setEditable(false);
		tfDataComanda.setEditable(false);
		tfDataL.setEditable(false);
		tfPorts.setEditable(false);

	}

	public void comobox() {
		ArrayList<String> noms = new ArrayList<>();
		for (Producte c : prova.getProductes()) {
			noms.add(c.getNomProducte());
		}

		// COGER LA COLUMNA DESEADA
		TableColumn col = tbComandes.getColumnModel().getColumn(2);
		// CREAR EL COMBOX
		JComboBox productes = new JComboBox();
		// LLENAR EL COMBOX DE NOMBRES DE PRODUCTES
		for (String nombre : noms) {
			productes.addItem(nombre);
		}

		// AÑADIR EL COMBOX A LA CELDA
		TableCellEditor tce = new DefaultCellEditor(productes);
		col.setCellEditor(tce);
	}

	public void comoboxClients() {
		ArrayList<String> noms = new ArrayList<>();
		for (Client c : prova.getClients()) {
			noms.add(c.getNomClient());
		}
		JComboBox clients = new JComboBox();
		for (String nombre : noms) {
			clients.addItem(nombre);
		}
		tfC.setText(clients.getSelectedItem().toString());
	}

	public Magatzem getMagatzem() {
		return prova;
	}
}