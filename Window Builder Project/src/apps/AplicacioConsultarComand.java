package apps;

import tastat.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingConstants;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import java.awt.Dialog.ModalityType;

import com.toedter.calendar.JDateChooser;
import java.awt.Dimension;

public class AplicacioConsultarComand extends JDialog {
	private Magatzem prova;
	private JComboBox<String> cbClient = new JComboBox<String>();
	private JComboBox<String> cbEstat = new JComboBox<String>();
	private JTable tbComanda;
	private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	private JDateChooser dateChooserVenciment, dateChooserExpedicio;

	/*public static void main(String[] args) {
		try {
			AplicacioConsultarComand dialog = new AplicacioConsultarComand();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public AplicacioConsultarComand(Magatzem mg) {
		prova = mg;
		this.setMinimumSize(new Dimension(730, 485));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.setTitle("Consultar Comandes");
		this.getContentPane().setLayout(null);

		JTextPane textTitol = new JTextPane();
		textTitol.setText("CONSULTAR COMANDES");
		textTitol.setForeground(Color.DARK_GRAY);
		textTitol.setFont(new Font("Arial Black", Font.BOLD, 32));
		textTitol.setEditable(false);
		textTitol.setBackground(SystemColor.inactiveCaption);
		textTitol.setBounds(0, 0, 727, 82);
		getContentPane().add(textTitol);

		JLabel lblNewLabel = new JLabel("Client:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setHorizontalAlignment(SwingConstants.LEFT);
		lblNewLabel.setBounds(62, 121, 42, 20);
		getContentPane().add(lblNewLabel);

		JLabel lblDataVenciment = new JLabel("Data Venciment: ");
		lblDataVenciment.setHorizontalAlignment(SwingConstants.LEFT);
		lblDataVenciment.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDataVenciment.setBounds(387, 200, 113, 20);
		getContentPane().add(lblDataVenciment);

		JLabel lblEstat = new JLabel("Estat:");
		lblEstat.setHorizontalAlignment(SwingConstants.LEFT);
		lblEstat.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblEstat.setBounds(387, 121, 42, 20);
		getContentPane().add(lblEstat);

		JLabel lblDataExpedicio = new JLabel("Data Expedicio:");
		lblDataExpedicio.setHorizontalAlignment(SwingConstants.LEFT);
		lblDataExpedicio.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDataExpedicio.setBounds(62, 200, 100, 20);
		getContentPane().add(lblDataExpedicio);

		comoboxClients();
		cbClient.setBounds(140, 122, 172, 20);
		getContentPane().add(cbClient);

		comoboxEstat();
		cbEstat.setBounds(465, 122, 172, 20);
		getContentPane().add(cbEstat);

		JButton btnClient = new JButton("Filtrar Per Client");
		btnClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				filtrarClient();
			}
		});
		btnClient.setBounds(175, 153, 137, 23);
		getContentPane().add(btnClient);

		JButton btnVenciment = new JButton("Filtrar Per Data Venciment");
		btnVenciment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filtrarVenciment();
			}
		});
		btnVenciment.setBounds(465, 231, 172, 23);
		getContentPane().add(btnVenciment);

		JButton btnFiltrarPerData = new JButton("Filtrar Per Data Expedicio");
		btnFiltrarPerData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				filtrarExpedicio();
			}
		});
		btnFiltrarPerData.setBounds(140, 231, 172, 23);
		getContentPane().add(btnFiltrarPerData);

		JButton btnFiltrarPerEstat = new JButton("Filtrar Per Estat");
		btnFiltrarPerEstat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				filtrarEstat();
			}
		});
		btnFiltrarPerEstat.setBounds(498, 153, 139, 23);
		getContentPane().add(btnFiltrarPerEstat);

		JPanel pComanda = new JPanel();
		pComanda.setBorder(new LineBorder(Color.LIGHT_GRAY));
		pComanda.setBounds(20, 292, 687, 140);
		getContentPane().add(pComanda);
		pComanda.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 687, 140);
		pComanda.add(scrollPane);

		tbComanda = new JTable();
		tbComanda.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Id Comanda", "Nom Client", "Data Expedicio", "Data Venciment", "Estat", "Portes" }) {
			Class[] columnTypes = new Class[] { Integer.class, String.class, String.class, String.class, String.class,
					Double.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tbComanda.getColumnModel().getColumn(2).setPreferredWidth(106);
		tbComanda.getColumnModel().getColumn(3).setPreferredWidth(96);
		scrollPane.setViewportView(tbComanda);

		dateChooserVenciment = new JDateChooser();
		dateChooserVenciment.setBounds(510, 200, 127, 20);
		getContentPane().add(dateChooserVenciment);

		dateChooserExpedicio = new JDateChooser();
		dateChooserExpedicio.setBounds(185, 200, 127, 20);
		getContentPane().add(dateChooserExpedicio);
	}

	private void filtrarExpedicio() {
		ArrayList<Comanda> comandes = new ArrayList<Comanda>();

		if (dateChooserExpedicio.getDate() != null) {
			String date = format.format(dateChooserExpedicio.getDate());

			for (Comanda c : prova.getComandes()) {
				if (format.format(c.getDataComanda()).equals(date)) {
					comandes.add(c);
				}
			}

			resetTablas();
			DefaultTableModel taulaComanda = (DefaultTableModel) tbComanda.getModel();

			for (Comanda com : comandes) {
				int idProducte = com.getIdComanda();
				String nomClient = com.getClient().getNomClient();
				String dataEntrada = format.format(com.getDataComanda());
				String dataExpedicio = format.format(com.getDataLliurament());
				ComandaEstat estat = com.getEstat();
				double portes = com.getPortes();
				taulaComanda.addRow(new Object[] { idProducte, nomClient, dataEntrada, dataExpedicio, estat, portes });
			}
		}

	}

	private void filtrarVenciment() {
		ArrayList<Comanda> comandes = new ArrayList<Comanda>();

		if (dateChooserVenciment.getDate() != null) {
			String date = format.format(dateChooserVenciment.getDate());
			for (Comanda c : prova.getComandes()) {
				if (format.format(c.getDataLliurament()).equals(date)) {
					comandes.add(c);
				}
			}

			resetTablas();
			DefaultTableModel taulaComanda = (DefaultTableModel) tbComanda.getModel();

			for (Comanda com : comandes) {
				int idProducte = com.getIdComanda();
				String nomClient = com.getClient().getNomClient();
				String dataEntrada = format.format(com.getDataComanda());
				String dataExpedicio = format.format(com.getDataLliurament());
				ComandaEstat estat = com.getEstat();
				double portes = com.getPortes();
				taulaComanda.addRow(new Object[] { idProducte, nomClient, dataEntrada, dataExpedicio, estat, portes });
			}
		}

	}

	private void filtrarEstat() {
		ArrayList<Comanda> comandes = new ArrayList<Comanda>();

		String e = cbEstat.getSelectedItem().toString();
		for (Comanda c : prova.getComandes()) {
			if (c.getEstat().toString().equals(e)) {
				comandes.add(c);
			}
		}

		resetTablas();
		DefaultTableModel taulaComanda = (DefaultTableModel) tbComanda.getModel();

		for (Comanda com : comandes) {
			int idProducte = com.getIdComanda();
			String nomClient = com.getClient().getNomClient();
			String dataEntrada = format.format(com.getDataComanda());
			String dataExpedicio = format.format(com.getDataLliurament());
			ComandaEstat estat = com.getEstat();
			double portes = com.getPortes();
			taulaComanda.addRow(new Object[] { idProducte, nomClient, dataEntrada, dataExpedicio, estat, portes });
		}
	}

	private void filtrarClient() {
		ArrayList<Comanda> comandes = new ArrayList<Comanda>();

		String client = cbClient.getSelectedItem().toString();
		for (Comanda c : prova.getComandes()) {
			if (c.getClient().getNomClient().equals(client)) {
				comandes.add(c);
			}
		}

		resetTablas();
		DefaultTableModel taulaComanda = (DefaultTableModel) tbComanda.getModel();

		for (Comanda com : comandes) {
			int idProducte = com.getIdComanda();
			String nomClient = com.getClient().getNomClient();
			String dataEntrada = format.format(com.getDataComanda());
			String dataExpedicio = format.format(com.getDataLliurament());
			ComandaEstat estat = com.getEstat();
			double portes = com.getPortes();
			taulaComanda.addRow(new Object[] { idProducte, nomClient, dataEntrada, dataExpedicio, estat, portes });
		}
	}

	public void resetTablas() {
		tbComanda.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "Id Comanda", "Nom Client", "Data Expedicio", "Data Venciment", "Estat", "Portes" }) {
			Class[] columnTypes = new Class[] { Integer.class, String.class, String.class, String.class, String.class,
					Double.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, false, false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tbComanda.getColumnModel().getColumn(2).setPreferredWidth(106);
		tbComanda.getColumnModel().getColumn(3).setPreferredWidth(96);

	}

	public void comoboxClients() {
		ArrayList<String> noms = new ArrayList<String>();
		for (Client c : prova.getClients()) {
			noms.add(c.getNomClient().toString());
		}

		for (String nombre : noms) {
			cbClient.addItem(nombre);
		}

	}

	public void comoboxEstat() {
		ArrayList<String> estat = new ArrayList<String>();

		estat.add(ComandaEstat.LLIURADA.toString());
		estat.add(ComandaEstat.PENDENT.toString());
		estat.add(ComandaEstat.PREPARADA.toString());
		estat.add(ComandaEstat.TRANSPORT.toString());

		for (String nombre : estat) {
			cbEstat.addItem(nombre);
		}

	}

	public ArrayList<String> nomsClients() {
		ArrayList<String> noms = new ArrayList<>();
		for (Client c : prova.getClients()) {
			noms.add(c.getNomClient().toString());
		}
		return noms;
	}

	public Magatzem getMagatzem() {
		return prova;
	}
}
