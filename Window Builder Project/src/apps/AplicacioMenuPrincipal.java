package apps;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;

import tastat.Magatzem;
import tastat.Programa;

import java.awt.Font;
import javax.swing.JFormattedTextField;
import java.awt.SystemColor;
import javax.swing.JButton;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.util.Collections;
import java.awt.event.ActionEvent;

public class AplicacioMenuPrincipal {

	private JFrame frameMenu;
	private Magatzem prova = Programa.generarMagatzemProva();
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AplicacioMenuPrincipal window = new AplicacioMenuPrincipal();
					window.frameMenu.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public AplicacioMenuPrincipal() {
		frameMenu = new JFrame();
		frameMenu.setBounds(100, 100, 438, 433);
		frameMenu.setResizable(false);
		frameMenu.setTitle("Menú Principal");
		frameMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameMenu.getContentPane().setLayout(null);
		
		JTextPane txtpnMenuPrincipal = new JTextPane();
		txtpnMenuPrincipal.setEditable(false);
		txtpnMenuPrincipal.setBackground(SystemColor.activeCaption);
		txtpnMenuPrincipal.setFont(new Font("Arial Black", Font.BOLD, 26));
		txtpnMenuPrincipal.setText("MENÚ PRINCIPAL");
		txtpnMenuPrincipal.setBounds(80, 11, 274, 43);
		frameMenu.getContentPane().add(txtpnMenuPrincipal);
		
		JTextPane colorPane = new JTextPane();
		colorPane.setEditable(false);
		colorPane.setBackground(SystemColor.activeCaption);
		colorPane.setBounds(0, 0, 434, 65);
		frameMenu.getContentPane().add(colorPane);
		
		JButton btnNewButton = new JButton("Consultar Productes");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameMenu.setVisible(false);
				
				JDialog window = new AplicacioConsultarProd(prova);
				window.setVisible(true);
				window.setLocationRelativeTo(null);

				prova = ((AplicacioConsultarProd) window).getMagatzem();
				frameMenu.setVisible(true);
				window.dispose();
			}
		});
		btnNewButton.setFont(new Font("Arial Black", Font.BOLD, 22));
		btnNewButton.setBounds(10, 76, 414, 47);
		frameMenu.getContentPane().add(btnNewButton);
		
		JButton btnModificarProductes = new JButton("Modificar Productes");
		btnModificarProductes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frameMenu.setVisible(false);
				
				//Aqui deja de mostrarse y se vuelve NO interactuable el menu principal
				//Se inicia la otra ventana
				
				JDialog window = new AplicacioModificarProducte(prova);
				window.setVisible(true);
				window.setLocationRelativeTo(null);
				
				//Aquí vuelve a visualizarse
				
				prova = ((AplicacioModificarProducte) window).getMagatzem();
				frameMenu.setVisible(true);
				window.dispose();
				
				
			}
		});
		
		btnModificarProductes.setFont(new Font("Arial Black", Font.BOLD, 22));
		btnModificarProductes.setBounds(10, 130, 414, 47);
		frameMenu.getContentPane().add(btnModificarProductes);
		
		JButton btnMantenimentClients = new JButton("Manteniment Clients");
		btnMantenimentClients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frameMenu.setVisible(false);
				
				JDialog window = new AplicacioMantenirClients(prova);
				window.setVisible(true);
				window.setLocationRelativeTo(null);

				prova = ((AplicacioMantenirClients) window).getMagatzem();
				frameMenu.setVisible(true);
				window.dispose();
			}
		});
		btnMantenimentClients.setFont(new Font("Arial Black", Font.PLAIN, 22));
		btnMantenimentClients.setBounds(10, 188, 414, 47);
		frameMenu.getContentPane().add(btnMantenimentClients);
		
		JButton btnConsultarComandes = new JButton("Consultar Comandes");
		btnConsultarComandes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frameMenu.setVisible(false);
				
				JDialog window = new AplicacioConsultarComand(prova);
				window.setVisible(true);
				window.setLocationRelativeTo(null);

				prova = ((AplicacioConsultarComand) window).getMagatzem();
				frameMenu.setVisible(true);
				window.dispose();
			}
		});
		btnConsultarComandes.setFont(new Font("Arial Black", Font.BOLD, 22));
		btnConsultarComandes.setBounds(10, 242, 414, 47);
		frameMenu.getContentPane().add(btnConsultarComandes);
		
		JButton btnMantenirComandes = new JButton("Mantenir Comandes");
		btnMantenirComandes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frameMenu.setVisible(false);
				
				JDialog window = new AplicacioMantenirComandes(prova);
				window.setVisible(true);
				window.setLocationRelativeTo(null);
				
				prova = ((AplicacioMantenirComandes) window).getMagatzem();
				frameMenu.setVisible(true);
				window.dispose();
			}
		});
		btnMantenirComandes.setFont(new Font("Arial Black", Font.BOLD, 22));
		btnMantenirComandes.setBounds(10, 296, 414, 47);
		frameMenu.getContentPane().add(btnMantenirComandes);
		
		JButton btnPrepararComandes = new JButton("Preparar Comandes");
		btnPrepararComandes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frameMenu.setVisible(false);
				
				JDialog window = new AplicacioPrepararComanda(prova);
				window.setVisible(true);
				window.setLocationRelativeTo(null);
				
				prova = ((AplicacioPrepararComanda) window).getMagatzem();
				frameMenu.setVisible(true);
				window.dispose();
			}
		});
		btnPrepararComandes.setFont(new Font("Arial Black", Font.BOLD, 22));
		btnPrepararComandes.setBounds(10, 350, 414, 47);
		frameMenu.getContentPane().add(btnPrepararComandes);
	}
}
