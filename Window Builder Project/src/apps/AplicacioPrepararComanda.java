package apps;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextPane;
import java.awt.SystemColor;
import java.awt.Dialog.ModalityType;
import java.util.ArrayList;
import java.util.Collections;
import java.awt.Font;
import java.util.List;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import tastat.*;

import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;

public class AplicacioPrepararComanda extends JDialog {
	private Magatzem prova;
	private JTextField txtNumComanda;
	private JTable tableComanda;
	private JTable tableStock;
	private JTextPane txtpnComanda;
	private JLabel lblError;
	private int countComandes = 0;
	private int MAX;
	private int id = 0;
	private JTextField txtEstat;
	
	/*public static void main(String[] args) {
		try {
			AplicacioPrepararComanda dialog = new AplicacioPrepararComanda();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public AplicacioPrepararComanda(Magatzem magatzem) {
		prova = magatzem;
		Collections.sort(prova.getComandes());
		Collections.sort(prova.getProductes());
		MAX = prova.getComandes().size();
		this.setMinimumSize(new Dimension(630, 450));
		this.setTitle("Preparar Comandes");
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setResizable(false);
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.getContentPane().setBackground(Color.WHITE);
		this.getContentPane().setLayout(null);
		
		JTextPane txtpnPrepararComandes = new JTextPane();
		txtpnPrepararComandes.setText("PREPARAR COMANDES");
		txtpnPrepararComandes.setForeground(Color.DARK_GRAY);
		txtpnPrepararComandes.setFont(new Font("Arial Black", Font.BOLD, 32));
		txtpnPrepararComandes.setEditable(false);
		txtpnPrepararComandes.setBackground(SystemColor.inactiveCaption);
		txtpnPrepararComandes.setBounds(0, 0, 627, 82);
		getContentPane().add(txtpnPrepararComandes);

		txtNumComanda = new JTextField();
		txtNumComanda.setFont(new Font("Arial", Font.BOLD, 22));
		txtNumComanda.setBounds(128, 93, 110, 23);
		getContentPane().add(txtNumComanda);
		txtNumComanda.setColumns(10);
		
		txtEstat = new JTextField();
		txtEstat.setEditable(false);
		txtEstat.setFont(new Font("Arial", Font.BOLD, 22));
		txtEstat.setColumns(10);
		txtEstat.setBounds(248, 93, 130, 23);
		getContentPane().add(txtEstat);

		txtpnComanda = new JTextPane();
		txtpnComanda.setEditable(false);
		txtpnComanda.setBackground(Color.WHITE);
		txtpnComanda.setFont(new Font("Arial", Font.BOLD, 16));
		txtpnComanda.setText("Comanda:");
		txtpnComanda.setBounds(10, 93, 108, 23);
		getContentPane().add(txtpnComanda);

		JButton buttonAnterior = new JButton("<");
		buttonAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				previousComanda();
			}
		});
		buttonAnterior.setBounds(388, 93, 50, 23);
		getContentPane().add(buttonAnterior);

		JButton buttonBuscar = new JButton("BUSCAR");
		buttonBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buscarComanda();
				
			}
		});
		buttonBuscar.setFont(new Font("Tahoma", Font.BOLD, 9));
		buttonBuscar.setBounds(441, 93, 110, 23);
		getContentPane().add(buttonBuscar);

		JButton buttonSeguent = new JButton(">");
		buttonSeguent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextComanda();
			}
		});
		buttonSeguent.setBounds(554, 93, 50, 23);
		getContentPane().add(buttonSeguent);

		JScrollPane scrollPaneContingutComanda = new JScrollPane();
		scrollPaneContingutComanda.setBounds(10, 127, 281, 233);
		getContentPane().add(scrollPaneContingutComanda);

		tableComanda = new JTable();
		scrollPaneContingutComanda.setViewportView(tableComanda);

		JScrollPane scrollPaneStock = new JScrollPane();
		scrollPaneStock.setBounds(301, 127, 303, 233);
		getContentPane().add(scrollPaneStock);

		tableStock = new JTable();
		scrollPaneStock.setViewportView(tableStock);
		
		resetTablas();
		
		JButton buttonPreparar = new JButton("PREPARAR COMANDA");
		buttonPreparar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prepararComanda();
			}
		});
		buttonPreparar.setFont(new Font("Tahoma", Font.BOLD, 11));
		buttonPreparar.setBounds(10, 371, 228, 28);
		getContentPane().add(buttonPreparar);

		lblError = new JLabel("");
		lblError.setForeground(Color.RED);
		lblError.setBackground(Color.WHITE);
		lblError.setBounds(301, 371, 303, 28);
		getContentPane().add(lblError);

	}

	protected void prepararComanda() {
		Comanda c = trobarComanda(id);
		boolean correcte = true;
		if(c != null) {
			if(c.getEstat().toString().equals("PENDENT")) {
				for(ComandaLinia cl: c.getLinies()) {
					for(Producte p: prova.getProductes()) {
						if(cl.getProducte().getCodiProducte() == p.getCodiProducte()) {
							if(p.getStock() < cl.getQuantitat()) {
								correcte = false;
								break;
							}
						}
					}
					if(!correcte) {
						break;
					}
				}
				
				if(!correcte) {
					lblError.setText("No es pot preparar, no hi ha suficient STOCK");
				} else {
					for(ComandaLinia cl: c.getLinies()) {
						for(Producte p: prova.getProductes()) {
							if(cl.getProducte().getCodiProducte() == p.getCodiProducte()) {
								p.setStock(p.getStock()-cl.getQuantitat());
							}
						}
					}
					c.setEstat(ComandaEstat.PREPARADA);
					mostrar(c);
					lblError.setText("Comanda Preparada!");
				}
				
			} else {
				lblError.setText("Només es preparen comandes PENDENTS");
			}
		} else {
			lblError.setText("No hi ha cap comanda seleccionada");
		}
	}

	private void resetTablas() {
		tableComanda.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Id Producte", "Nom Producte", "Quantitat" }) {
					Class[] columnTypes = new Class[] { Integer.class, String.class, Integer.class };

					public Class getColumnClass(int columnIndex) {
						return columnTypes[columnIndex];
					}

					boolean[] columnEditables = new boolean[] { false, false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});
		tableComanda.getColumnModel().getColumn(0).setPreferredWidth(78);
		tableComanda.getColumnModel().getColumn(1).setPreferredWidth(92);
		tableComanda.setRowSelectionAllowed(false);

		tableStock.setModel(new DefaultTableModel(new Object[][] {},new String[] { "Id Producte", "Nom Producte", "Stock", "Stock M\u00EDnim" }) {
			Class[] columnTypes = new Class[] { Integer.class, String.class, Integer.class, Integer.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}

			boolean[] columnEditables = new boolean[] { false, false, false, false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		tableStock.getColumnModel().getColumn(1).setPreferredWidth(81);
		tableStock.setRowSelectionAllowed(false);
		
		DefaultTableModel taulaStock = (DefaultTableModel) tableStock.getModel();
		
		for(Producte p: prova.getProductes()) {
			if(p.getTipus().toString().equals("VENDIBLE")) {
				int id = p.getCodiProducte();
				String nom = p.getNomProducte();
				int stock = p.getStock();
				int stockminim = p.getStockMinim();
				
				taulaStock.addRow(new Object[] { id, nom, stock, stockminim });
			}
		}
	}

	private void mostrar(Comanda c) {
		txtNumComanda.setText(Integer.toString(id));
		txtEstat.setText(c.getEstat().toString());
		resetTablas();
		
		DefaultTableModel taulaComanda = (DefaultTableModel) tableComanda.getModel();
		
		List<ComandaLinia> linies = c.getLinies();
		
		if(linies != null) {
			for(ComandaLinia cl: linies) {
				int id = cl.getProducte().getCodiProducte();
				String nom = cl.getProducte().getNomProducte();
				int quant = cl.getQuantitat();
				
				taulaComanda.addRow(new Object[] { id, nom, quant });
			}
		}
		
	}
	
	private Comanda trobarComanda(int id) {
		for(Comanda c: prova.getComandes()) {
			if(c.getIdComanda() == id) {
				return c;
			}
		}
		return null;
	}

	private void buscarComanda() {
		Comanda c = null;
		
		if(!txtNumComanda.getText().equals("") && txtNumComanda.getText().matches("^[0-9]+$")) {
			id = Integer.parseInt(txtNumComanda.getText());
			c = trobarComanda(id);
			
			if(c == null) {
				lblError.setText("No existeix cap comanda amb aquesta ID");
				countComandes = 0;
				id = 0;
			} else {
				int contador = 0;
				mostrar(c);
				for(Comanda com: prova.getComandes()) {
					contador++;
					if(com.getIdComanda() == c.getIdComanda()) {
						break;
					}
				}
				countComandes = contador;
			}
		} else {
			if (txtNumComanda.getText().equals("")) {
				lblError.setText("Introdueix una ID");
			} else {
				lblError.setText("Només es poden introduir numeros!");
			}
		}
	}

	private void nextComanda() {
		Comanda c = null;

		if ((countComandes + 1) <= MAX) {
			do {
				id++;
				c = trobarComanda(id);
			} while (c == null);
			countComandes++;
			mostrar(c);
		} else if (MAX == 0) {
			lblError.setText("No hi ha Comandes...");
		}
	}

	private void previousComanda() {
		Comanda c = null;

		if ((countComandes - 1) > 0) {
			do {
				id--;
				c = trobarComanda(id);
				countComandes--;
			} while (c == null);
			c = trobarComanda(id);
			mostrar(c);
		} else if (countComandes == 0 && MAX != 0) {
			id = prova.getComandes().get(0).getIdComanda();
			countComandes++;
			c = trobarComanda(id);
			mostrar(c);
		} else if (MAX == 0) {
			lblError.setText("No hi ha Comandes...");
		}
	}

	public Magatzem getMagatzem() {
		return prova;
	}
}
