package apps;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JTextPane;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Window;
import java.awt.Dialog.ModalityType;

import javax.swing.JRadioButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import dialog.DialogAfegirLots;
import dialog.DialogAfegirModComp;
import dialog.DialogAfegirModProducte;
import dialog.DialogModLots;
import tastat.Generador;
import tastat.LotDesglossat;
import tastat.Magatzem;
import tastat.Producte;
import tastat.Programa;
import tastat.Proveidor;
import tastat.Tipus;
import tastat.UnitatMesura;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.ListSelectionModel;
import java.awt.SystemColor;
import java.awt.Dialog.ModalExclusionType;

public class AplicacioModificarProducte extends JDialog{

	private int id = 0;
	private int countProd = 0;
	private int MAX;
	private Magatzem prova;
	private JTextField textIdProd;
	private JTextField textNom;
	private JTextField textPreuVenda;
	private JTextField textStock;
	private JTextField textStockMinim;
	private JTextField textTipus;
	private JTextField textUnitat;
	private JLabel textErrorPanel, textErrorPanel2;
	private JTable taulaLots;
	private JTable taulaComposicio;
	private SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	private JTextField textProveidor;

	/*public static void main(String[] args) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				AplicacioProducte window = new AplicacioProducte(prova);
				window.frmAppProducte.setVisible(true);
			}
		});
	}*/

	public AplicacioModificarProducte(Magatzem magatzem) {		
		prova = magatzem;
		Collections.sort(prova.getProductes());
		MAX = prova.getProductes().size();
		this.setModalityType(ModalityType.APPLICATION_MODAL);
		this.setResizable(false);
		this.getContentPane().setBackground(Color.WHITE);
		this.setTitle("App Producte");
		this.setBounds(100, 100, 720, 540);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JTextPane txtpnModificarProductes = new JTextPane();
		txtpnModificarProductes.setText("MODIFICAR PRODUCTES");
		txtpnModificarProductes.setForeground(Color.DARK_GRAY);
		txtpnModificarProductes.setBackground(SystemColor.inactiveCaption);
		txtpnModificarProductes.setEditable(false);
		txtpnModificarProductes.setFont(new Font("Arial Black", Font.BOLD, 32));
		txtpnModificarProductes.setBounds(0, 0, 714, 82);
		this.getContentPane().add(txtpnModificarProductes);

		JPanel panelBuscador = new JPanel();
		panelBuscador.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panelBuscador.setBackground(Color.WHITE);
		JPanel panelComposicio = new JPanel();
		JPanel panelLots = new JPanel();
		Collections.sort(prova.getProductes());
		panelBuscador.setBounds(10, 91, 288, 361);
		panelBuscador.setLayout(null);

		JTextPane txtpnIdproducte = new JTextPane();
		txtpnIdproducte.setBackground(Color.WHITE);
		txtpnIdproducte.setBounds(41, 11, 95, 20);
		panelBuscador.add(txtpnIdproducte);
		txtpnIdproducte.setEditable(false);
		txtpnIdproducte.setText("idProducte:");

		JTextPane txtpnNom = new JTextPane();
		txtpnNom.setBackground(Color.WHITE);
		txtpnNom.setBounds(41, 36, 95, 20);
		panelBuscador.add(txtpnNom);
		txtpnNom.setText("Nom:");
		txtpnNom.setEditable(false);

		JTextPane txtpnPreuVenda = new JTextPane();
		txtpnPreuVenda.setBackground(Color.WHITE);
		txtpnPreuVenda.setBounds(41, 61, 95, 20);
		panelBuscador.add(txtpnPreuVenda);
		txtpnPreuVenda.setText("Preu Venda:");
		txtpnPreuVenda.setEditable(false);

		JTextPane txtpnStock = new JTextPane();
		txtpnStock.setBackground(Color.WHITE);
		txtpnStock.setBounds(41, 86, 95, 20);
		panelBuscador.add(txtpnStock);
		txtpnStock.setText("Stock:");
		txtpnStock.setEditable(false);

		JTextPane txtpnStockMnim = new JTextPane();
		txtpnStockMnim.setBackground(Color.WHITE);
		txtpnStockMnim.setBounds(41, 111, 95, 20);
		panelBuscador.add(txtpnStockMnim);
		txtpnStockMnim.setText("Stock Mínim:");
		txtpnStockMnim.setEditable(false);

		JTextPane txtpnTipus = new JTextPane();
		txtpnTipus.setBackground(Color.WHITE);
		txtpnTipus.setText("Tipus Producte:");
		txtpnTipus.setEditable(false);
		txtpnTipus.setBounds(41, 136, 95, 20);
		panelBuscador.add(txtpnTipus);

		JTextPane txtpnUnitat = new JTextPane();
		txtpnUnitat.setBackground(Color.WHITE);
		txtpnUnitat.setText("Unitat Mesura:");
		txtpnUnitat.setEditable(false);
		txtpnUnitat.setBounds(41, 161, 95, 20);
		panelBuscador.add(txtpnUnitat);

		JTextPane txtpnProveidor = new JTextPane();
		txtpnProveidor.setText("Proveidor:");
		txtpnProveidor.setEditable(false);
		txtpnProveidor.setBackground(Color.WHITE);
		txtpnProveidor.setBounds(41, 186, 95, 20);
		panelBuscador.add(txtpnProveidor);

		textErrorPanel = new JLabel("");
		textErrorPanel.setBounds(16, 304, 250, 14);
		panelBuscador.add(textErrorPanel);
		textErrorPanel.setHorizontalAlignment(SwingConstants.CENTER);
		textErrorPanel.setForeground(Color.RED);

		textErrorPanel2 = new JLabel("");
		textErrorPanel2.setBounds(16, 320, 250, 15);
		panelBuscador.add(textErrorPanel2);
		textErrorPanel2.setHorizontalAlignment(SwingConstants.CENTER);
		textErrorPanel2.setForeground(Color.RED);

		textIdProd = new JTextField();
		textIdProd.setAlignmentX(Component.RIGHT_ALIGNMENT);
		textIdProd.setBounds(141, 11, 95, 20);
		panelBuscador.add(textIdProd);
		textIdProd.setColumns(10);

		textNom = new JTextField();
		textNom.setAlignmentX(Component.RIGHT_ALIGNMENT);
		textNom.setBounds(141, 36, 132, 20);
		panelBuscador.add(textNom);
		textNom.setEditable(false);
		textNom.setColumns(10);

		textPreuVenda = new JTextField();
		textPreuVenda.setAlignmentX(Component.RIGHT_ALIGNMENT);
		textPreuVenda.setBounds(141, 61, 95, 20);
		panelBuscador.add(textPreuVenda);
		textPreuVenda.setEditable(false);
		textPreuVenda.setColumns(10);

		textStock = new JTextField();
		textStock.setAlignmentX(Component.RIGHT_ALIGNMENT);
		textStock.setBounds(141, 86, 95, 20);
		panelBuscador.add(textStock);
		textStock.setEditable(false);
		textStock.setColumns(10);

		textStockMinim = new JTextField();
		textStockMinim.setAlignmentX(Component.RIGHT_ALIGNMENT);
		textStockMinim.setBounds(141, 111, 95, 20);
		panelBuscador.add(textStockMinim);
		textStockMinim.setEditable(false);
		textStockMinim.setColumns(10);

		textTipus = new JTextField();
		textTipus.setEditable(false);
		textTipus.setColumns(10);
		textTipus.setAlignmentX(1.0f);
		textTipus.setBounds(141, 136, 95, 20);
		panelBuscador.add(textTipus);

		textUnitat = new JTextField();
		textUnitat.setEditable(false);
		textUnitat.setColumns(10);
		textUnitat.setAlignmentX(1.0f);
		textUnitat.setBounds(141, 161, 95, 20);
		panelBuscador.add(textUnitat);

		textProveidor = new JTextField();
		textProveidor.setEditable(false);
		textProveidor.setColumns(10);
		textProveidor.setAlignmentX(1.0f);
		textProveidor.setBounds(141, 186, 95, 20);
		panelBuscador.add(textProveidor);

		JButton btnBuscar = new JButton("BUSCAR");
		btnBuscar.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buscarProd();
			}
		});
		btnBuscar.setBounds(92, 220, 89, 23);
		panelBuscador.add(btnBuscar);

		JButton btnAnterior = new JButton("<");
		btnAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				previousProd();
			}
		});
		btnAnterior.setBounds(39, 220, 50, 23);
		panelBuscador.add(btnAnterior);

		JButton btnSeguent = new JButton(">");
		btnSeguent.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nextProd();
			}
		});
		btnSeguent.setBounds(184, 220, 50, 23);
		panelBuscador.add(btnSeguent);

		JButton btnAfegirProd = new JButton("AFEGIR");
		btnAfegirProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				afegirProducte();
			}
		});
		btnAfegirProd.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnAfegirProd.setBounds(2, 247, 87, 23);
		panelBuscador.add(btnAfegirProd);

		JButton btnTreureProd = new JButton("TREURE");
		btnTreureProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				treureProducte();
			}
		});
		btnTreureProd.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnTreureProd.setBounds(92, 247, 89, 23);
		panelBuscador.add(btnTreureProd);

		JButton btnModProd = new JButton("MODIFICAR");
		btnModProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				modificarProducte();
			}
		});
		btnModProd.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnModProd.setBounds(184, 247, 102, 23);
		panelBuscador.add(btnModProd);

		panelComposicio.setBackground(Color.WHITE);
		panelComposicio.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Composicio",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelComposicio.setBounds(308, 91, 399, 191);
		panelComposicio.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setBorder(new LineBorder(new Color(130, 135, 144), 2));
		scrollPane.setBounds(6, 16, 385, 140);
		panelComposicio.add(scrollPane);

		taulaComposicio = new JTable();
		taulaComposicio.setBackground(Color.WHITE);
		taulaComposicio.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		taulaComposicio.setModel(
				new DefaultTableModel(new Object[][] {}, new String[] { "id Producte", "Nom Producte", "Quantitat" }) {
					Class[] columnTypes = new Class[] { Integer.class, String.class, Integer.class };

					public Class getColumnClass(int columnIndex) {
						return columnTypes[columnIndex];
					}

					boolean[] columnEditables = new boolean[] { false, false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});
		taulaComposicio.getColumnModel().getColumn(0).setResizable(false);
		taulaComposicio.getColumnModel().getColumn(0).setPreferredWidth(79);
		taulaComposicio.getColumnModel().getColumn(1).setResizable(false);
		taulaComposicio.getColumnModel().getColumn(1).setPreferredWidth(100);
		taulaComposicio.getColumnModel().getColumn(2).setResizable(false);
		scrollPane.setViewportView(taulaComposicio);

		JButton btnAfegirComposicio = new JButton("AFEGIR/MODIFICAR");
		btnAfegirComposicio.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnAfegirComposicio.setBounds(6, 159, 144, 23);
		btnAfegirComposicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				afegirComp();
			}
		});
		panelComposicio.add(btnAfegirComposicio);

		JButton btnTreureComposicio = new JButton("TREURE");
		btnTreureComposicio.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnTreureComposicio.setBounds(153, 159, 89, 23);
		btnTreureComposicio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int i = taulaComposicio.getSelectedRow();
				DefaultTableModel model = (DefaultTableModel) taulaComposicio.getModel();

				if (i != -1) {
					int id = (int) model.getValueAt(i, 0);
					Producte prod = trobarProd();

					Map<Producte, Integer> comp = prod.getComposicio();
					Set<Producte> composicio = comp.keySet();

					composicio.remove(trobarProd(id));

					model.removeRow(i);
				}
			}
		});
		panelComposicio.add(btnTreureComposicio);

		panelLots.setBackground(Color.WHITE);
		panelLots.setLayout(null);
		panelLots.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Lots", TitledBorder.LEADING,
				TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelLots.setBounds(308, 293, 399, 207);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBorder(new LineBorder(new Color(130, 135, 144), 2));
		scrollPane_1.setBounds(6, 16, 385, 159);
		panelLots.add(scrollPane_1);

		taulaLots = new JTable();
		taulaLots.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		taulaLots.setBackground(Color.WHITE);
		taulaLots.setForeground(Color.BLACK);
		taulaLots.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "id Lot", "Data Entrada", "Data Caducitat", "Quantitat" }) {
			Class[] columnTypes = new Class[] { Integer.class, String.class, String.class, Integer.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		taulaLots.getColumnModel().getColumn(0).setResizable(false);
		taulaLots.getColumnModel().getColumn(1).setResizable(false);
		taulaLots.getColumnModel().getColumn(2).setResizable(false);
		taulaLots.getColumnModel().getColumn(3).setResizable(false);
		taulaLots.setBorder(new LineBorder(Color.LIGHT_GRAY));
		scrollPane_1.setViewportView(taulaLots);

		JButton btnAfegirLots = new JButton("AFEGIR");
		btnAfegirLots.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnAfegirLots.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				afegirLot();
			}
		});
		btnAfegirLots.setBounds(6, 177, 89, 23);
		panelLots.add(btnAfegirLots);

		JButton btnTreureLots = new JButton("TREURE");
		btnTreureLots.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnTreureLots.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = taulaLots.getSelectedRow();
				DefaultTableModel model = (DefaultTableModel) taulaLots.getModel();

				if (i != -1) {
					int id = (int) model.getValueAt(i, 0);
					Producte prod = trobarProd();

					List<LotDesglossat> lots = prod.getLots();
					LotDesglossat lot = null;

					for (LotDesglossat l : lots) {
						if (l.getLot() == id) {
							lot = l;
							break;
						}
					}

					lots.remove(lot);
					model.removeRow(i);
					mostrar(prod);
				}
			}
		});
		btnTreureLots.setBounds(98, 177, 89, 23);
		panelLots.add(btnTreureLots);

		JButton btnModLot = new JButton("MODIFICAR");
		btnModLot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				modLots();
			}
		});
		btnModLot.setFont(new Font("Tahoma", Font.BOLD, 9));
		btnModLot.setBounds(190, 177, 104, 23);
		panelLots.add(btnModLot);

		this.getContentPane().setLayout(null);

		this.getContentPane().add(panelBuscador);
		this.getContentPane().add(panelComposicio);
		this.getContentPane().add(panelLots);
	}

	private void buscarProd() {
		Producte prod = null;

		if (!textIdProd.getText().equals("") && textIdProd.getText().matches("^[0-9]+$")) {
			id = Integer.parseInt(textIdProd.getText());
			prod = trobarProd();
			if (prod == null) {
				textErrorPanel.setText("No existeix cap Producte");
				textErrorPanel2.setText("amb ID Producte " + id);
				id = 0;
				countProd = 0;
			} else {
				int contador = 0;
				mostrar(prod);
				for (Producte p : prova.getProductes()) {
					contador++;
					if (p.getCodiProducte() == prod.getCodiProducte()) {
						break;
					}
				}
				countProd = contador;
			}
		} else {
			if (textIdProd.getText().equals("")) {
				textErrorPanel.setText("Introdueix una ID o");
				textErrorPanel2.setText("navega amb els botons < o >");
			} else {
				textErrorPanel.setText("Només es poden");
				textErrorPanel2.setText("introduir numeros!");
			}
		}
	}

	private void nextProd() {
		Producte p = null;

		if ((countProd + 1) <= MAX) {
			do {
				id++;
				p = trobarProd();
			} while (p == null);
			countProd++;
			mostrar(p);
		} else if (MAX == 0) {
			textErrorPanel.setForeground(Color.RED);
			textErrorPanel2.setForeground(Color.RED);
			textErrorPanel.setText("No hi ha Productes");
			textErrorPanel2.setText("al Magatzem!");
		}
	}

	private void previousProd() {
		Producte p = null;

		if ((countProd - 1) > 0) {
			do {
				id--;
				p = trobarProd();
				countProd--;
			} while (p == null);
			p = trobarProd();
			mostrar(p);
		} else if (countProd == 0 && MAX != 0) {
			id = prova.getProductes().get(0).getCodiProducte();
			countProd++;
			p = trobarProd();
			mostrar(p);
		} else if (MAX == 0) {
			textErrorPanel.setForeground(Color.RED);
			textErrorPanel2.setForeground(Color.RED);
			textErrorPanel.setText("No hi ha Productes");
			textErrorPanel2.setText("al Magatzem!");
		}
	}

	private void modificarProducte() {
		Producte producte = trobarProd();
		if (producte != null) {
			JDialog dialogPanel = new DialogAfegirModProducte(prova, id);
			dialogPanel.setLocationRelativeTo(null);
			dialogPanel.setVisible(true);

			if (((DialogAfegirModProducte) dialogPanel).isCorrecte()) {
				String nomProd = ((DialogAfegirModProducte) dialogPanel).getTextNom().getText();
				double preuVenda = (double) ((DialogAfegirModProducte) dialogPanel).getSpinnerPreuVenda().getValue();
				int stockMinim = (int) ((DialogAfegirModProducte) dialogPanel).getSpinnerStockMinim().getValue();
				Tipus tipusProd;
				UnitatMesura unitatMesura;
				Proveidor proveidor = null;
				if (((DialogAfegirModProducte) dialogPanel).getComboTipusProd().getSelectedItem().equals("INGREDIENT")) {
					tipusProd = Tipus.INGREDIENT;
				} else {
					tipusProd = Tipus.VENDIBLE;
				}

				if (((DialogAfegirModProducte) dialogPanel).getComboUnitatMesura().getSelectedItem().equals("GRAMS")) {
					unitatMesura = UnitatMesura.GRAMS;
				} else if (((DialogAfegirModProducte) dialogPanel).getComboUnitatMesura().getSelectedItem()
						.equals("LLITRE")) {
					unitatMesura = UnitatMesura.LLITRE;
				} else {
					unitatMesura = UnitatMesura.UNITAT;
				}

				if (!((DialogAfegirModProducte) dialogPanel).getComboProveidor().getSelectedItem().equals(null)) {
					for (Proveidor prov : prova.getProveidors()) {
						if (prov.getNomProveidor()
								.equals(((DialogAfegirModProducte) dialogPanel).getComboProveidor().getSelectedItem())) {
							proveidor = prov;
							break;
						}
					}
				} else {
					proveidor = null;
				}

				Producte p = trobarProd();

				p.setNomProducte(nomProd);
				p.setPreuVenda(preuVenda);
				p.setStockMinim(stockMinim);
				p.setTipus(tipusProd);
				p.setUnitatMesura(unitatMesura);
				p.setProveidor(proveidor);

				Collections.sort(prova.getProductes());
				int contador = 0;
				mostrar(p);
				for (Producte prod : prova.getProductes()) {
					contador++;
					if (p.getCodiProducte() == prod.getCodiProducte()) {
						break;
					}
				}
				countProd = contador;
				id = p.getCodiProducte();

			} else {
				textErrorPanel.setForeground(Color.RED);
				textErrorPanel2.setForeground(Color.RED);
				textErrorPanel.setText("No s'ha modificat el Producte!");
				textErrorPanel2.setText("Faltaven camps per omplir o s'ha cancelat...");
			}

			dialogPanel.dispose();
			
		} else {
			textErrorPanel.setForeground(Color.RED);
			textErrorPanel2.setForeground(Color.RED);
			textErrorPanel.setText("No hi ha cap Producte");
			textErrorPanel2.setText("seleccionat ara mateix...");
		}
	}

	private void treureProducte() {
		Producte p = trobarProd();
		if (p != null) {
			prova.getProductes().remove(p);
			MAX = prova.getProductes().size();
			countProd = 0;
			textIdProd.setText("");
			textNom.setText("");
			textPreuVenda.setText("");
			textStock.setText("");
			textStockMinim.setText("");
			textTipus.setText("");
			textUnitat.setText("");
			textProveidor.setText("");

			taulaComposicio.setModel(new DefaultTableModel(new Object[][] {},
					new String[] { "id Producte", "Nom Producte", "Quantitat" }) {
				Class[] columnTypes = new Class[] { Integer.class, String.class, Integer.class };

				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}

				boolean[] columnEditables = new boolean[] { false, false, false };

				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});

			taulaLots.setModel(new DefaultTableModel(new Object[][] {},
					new String[] { "id Lot", "Data Entrada", "Data Caducitat", "Quantitat" }) {
				Class[] columnTypes = new Class[] { Integer.class, String.class, String.class, Integer.class };

				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
			});
			textErrorPanel.setForeground(Color.GREEN);
			textErrorPanel2.setForeground(Color.GREEN);
			textErrorPanel.setText("S'ha eliminat el Producte");
			textErrorPanel2.setText(p.getNomProducte() + "amb ID " + p.getCodiProducte());

		} else {
			textErrorPanel.setForeground(Color.RED);
			textErrorPanel2.setForeground(Color.RED);
			textErrorPanel.setText("No hi ha cap Producte");
			textErrorPanel2.setText("seleccionat ara mateix...");
		}

	}

	private void afegirProducte() {
		int idProd = Generador.getNextProducte();
		JDialog dialogPanel = new DialogAfegirModProducte(prova, idProd);
		dialogPanel.setLocationRelativeTo(null);
		dialogPanel.setVisible(true);

		if (((DialogAfegirModProducte) dialogPanel).isCorrecte()) {
			String nomProd = ((DialogAfegirModProducte) dialogPanel).getTextNom().getText();
			double preuVenda = (double) ((DialogAfegirModProducte) dialogPanel).getSpinnerPreuVenda().getValue();
			int stockMinim = (int) ((DialogAfegirModProducte) dialogPanel).getSpinnerStockMinim().getValue();
			Tipus tipusProd;
			UnitatMesura unitatMesura;
			Proveidor proveidor = null;
			if (((DialogAfegirModProducte) dialogPanel).getComboTipusProd().getSelectedItem().equals("INGREDIENT")) {
				tipusProd = Tipus.INGREDIENT;
			} else {
				tipusProd = Tipus.VENDIBLE;
			}

			if (((DialogAfegirModProducte) dialogPanel).getComboUnitatMesura().getSelectedItem().equals("GRAMS")) {
				unitatMesura = UnitatMesura.GRAMS;
			} else if (((DialogAfegirModProducte) dialogPanel).getComboUnitatMesura().getSelectedItem().equals("LLITRE")) {
				unitatMesura = UnitatMesura.LLITRE;
			} else {
				unitatMesura = UnitatMesura.UNITAT;
			}

			if (!((DialogAfegirModProducte) dialogPanel).getComboProveidor().getSelectedItem().equals(null)) {
				for (Proveidor prov : prova.getProveidors()) {
					if (prov.getNomProveidor()
							.equals(((DialogAfegirModProducte) dialogPanel).getComboProveidor().getSelectedItem())) {
						proveidor = prov;
						break;
					}
				}
			} else {
				proveidor = null;
			}

			Producte p = new Producte(idProd, nomProd, 0, stockMinim, unitatMesura, tipusProd, proveidor, preuVenda);
			prova.add(p);
			Collections.sort(prova.getProductes());
			MAX = prova.getProductes().size();
			countProd = MAX;
			mostrar(p);
			id = p.getCodiProducte();

		} else {
			Generador.setProducteActual(Generador.getProducteActual() - 1);
			textErrorPanel.setForeground(Color.RED);
			textErrorPanel2.setForeground(Color.RED);
			textErrorPanel.setText("No s'ha afegit el Producte!");
			textErrorPanel2.setText("Faltaven camps per omplir o s'ha cancelat...");
		}

		dialogPanel.dispose();
	}

	private void afegirLot() {
		Producte p = trobarProd();
		int idLot = Generador.getNextLot();
		JDialog dialogPanel = new DialogAfegirLots(prova, id, idLot);
		dialogPanel.setLocationRelativeTo(null);
		dialogPanel.setVisible(true);

		if (((DialogAfegirLots) dialogPanel).isCorrecte()) {
			Date dataCad = ((DialogAfegirLots) dialogPanel).getDataCad();
			int quant = (int) ((DialogAfegirLots) dialogPanel).getSpinnerQuant().getValue();
			LotDesglossat l = new LotDesglossat(idLot, dataCad, quant);

			p.getLots().add(l);
			mostrar(p);

		} else {
			Generador.setLotActual(Generador.getLotActual() - 1);
			textErrorPanel.setText("No s'ha afegit el Lot!");
			textErrorPanel2.setText("Faltaven camps per omplir...");
		}

		dialogPanel.dispose();
	}

	private void modLots() {
		Producte p = trobarProd();

		int i = taulaLots.getSelectedRow();
		DefaultTableModel model = (DefaultTableModel) taulaLots.getModel();

		if (i != -1) {
			int idLot = (int) model.getValueAt(i, 0);

			List<LotDesglossat> lots = p.getLots();
			LotDesglossat lot = null;

			for (LotDesglossat l : lots) {
				if (l.getLot() == idLot) {
					lot = l;
					break;
				}
			}

			JDialog dialogPanel = new DialogModLots(prova, id, lot);
			dialogPanel.setLocationRelativeTo(null);
			dialogPanel.setVisible(true);

			if (((DialogModLots) dialogPanel).isCorrecte()) {
				Date dataEnt = ((DialogModLots) dialogPanel).getDataEnt();
				Date dataCad = ((DialogModLots) dialogPanel).getDataCad();
				int quant = (int) ((DialogModLots) dialogPanel).getSpinnerQuant().getValue();

				lot.setQuantitat(quant);
				lot.setDataCaducitat(dataCad);
				lot.setDataEntrada(dataEnt);

				mostrar(p);
			}

			dialogPanel.dispose();

		}
	}

	private void afegirComp() {
		Producte p = trobarProd();
		if (p.getTipus() == Tipus.VENDIBLE) {

			JDialog dialogPanel = new DialogAfegirModComp(prova, id);
			dialogPanel.setLocationRelativeTo(null);
			dialogPanel.setVisible(true);
			if (((DialogAfegirModComp) dialogPanel).getCorrecte()) {

				int idProd = (int) ((DialogAfegirModComp) dialogPanel).getComboID().getSelectedItem();
				int quant = (int) ((DialogAfegirModComp) dialogPanel).getSpinQuantitat().getValue();

				Producte component = trobarProd(idProd);

				p.afegirComponent(component, quant);

				mostrar(p);
			}
			dialogPanel.dispose();

		} else {
			textErrorPanel.setForeground(Color.RED);
			textErrorPanel2.setForeground(Color.RED);
			textErrorPanel.setText("No es poden afegir components a");
			textErrorPanel2.setText("Productes de tipus INGREDIENT");
		}

	}

	public Producte trobarProd() {
		for (Producte p : prova.getProductes()) {
			if (p.getCodiProducte() == id) {
				return p;
			}
		}

		return null;
	}

	public Producte trobarProd(int id) {
		for (Producte p : prova.getProductes()) {
			if (p.getCodiProducte() == id) {
				return p;
			}
		}

		return null;
	}

	public void mostrar(Producte prod) {
		textErrorPanel.setForeground(Color.RED);
		textErrorPanel2.setForeground(Color.RED);
		textErrorPanel.setText("");
		textErrorPanel2.setText("");
		textIdProd.setText(Integer.toString(prod.getCodiProducte()));
		textNom.setText(prod.getNomProducte());
		textPreuVenda.setText(Double.toString(prod.getPreuVenda()));
		textStock.setText(Integer.toString(prod.getStock()));
		textStockMinim.setText(Integer.toString(prod.getStockMinim()));
		textTipus.setText(prod.getTipus().toString());
		textUnitat.setText(prod.getUnitatMesura().toString());

		if (prod.getProveidor() != null)
			textProveidor.setText(prod.getProveidor().getNomProveidor());
		else
			textProveidor.setText("No té proveidor");

		Map<Producte, Integer> comp = prod.getComposicio();
		Set<Producte> composicio = comp.keySet();

		List<LotDesglossat> lots = prod.getLots();

		// RESET TABLAS

		taulaComposicio.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "id Producte", "Nom Producte", "Quantitat" }) {
					Class[] columnTypes = new Class[] { Integer.class, String.class, Integer.class };

					public Class getColumnClass(int columnIndex) {
						return columnTypes[columnIndex];
					}

					boolean[] columnEditables = new boolean[] { false, false, false };

					public boolean isCellEditable(int row, int column) {
						return columnEditables[column];
					}
				});

		taulaLots.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "id Lot", "Data Entrada", "Data Caducitat", "Quantitat" }) {
			Class[] columnTypes = new Class[] { Integer.class, String.class, String.class, Integer.class };

			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});

		// RESET TABLAS

		DefaultTableModel taulaComp = (DefaultTableModel) taulaComposicio.getModel();
		DefaultTableModel taulaLot = (DefaultTableModel) taulaLots.getModel();

		if (composicio!= null) {
			for (Producte p : composicio) {
				int idProd = p.getCodiProducte();
				String nomProd = p.getNomProducte();
				int quantProd = prod.getComposicio().get(p);

				taulaComp.addRow(new Object[] { idProd, nomProd, quantProd });
			}
		}

		if (lots != null) {
			for (LotDesglossat l : lots) {
				int idLot = l.getLot();
				String dataEntrada = format.format(l.getDataEntrada());
				String dataCad = format.format(l.getDataCaducitat());
				int quantitat = l.getQuantitat();

				taulaLot.addRow(new Object[] { idLot, dataEntrada, dataCad, quantitat });
			}
		}
	}

	public Magatzem getMagatzem() {
		return prova;
	}

	public int getId() {
		return id;
	}

}
