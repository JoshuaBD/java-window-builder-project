package tastat;

import java.util.Set;

public class Proveidor{
	protected int idProveidor;
	protected String nomProveidor;
	protected String CIF;
	protected boolean actiu;
	protected String direccio;
	protected String poblacio;
	protected String pais;
	protected String personaContacte;
	protected String telefon;
	protected double latitud;
	protected double longitud;	
	Proveidor(){
		idProveidor = Generador.getNextProveidor();
	}
	
	Proveidor(String nom){
		this();
		nomProveidor = nom;
	}

	public int getIdProveidor() {
		return idProveidor;
	}

	public void setIdProveidor(int idProveidor) {
		this.idProveidor = idProveidor;
	}

	public String getNomProveidor() {
		return nomProveidor;
	}

	public void setNomProveidor(String nomProveidor) {
		this.nomProveidor = nomProveidor;
	}
	
	
	
}