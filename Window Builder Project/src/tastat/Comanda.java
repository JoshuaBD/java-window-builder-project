package tastat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Comanda implements Comparable<Comanda>{
	protected int idComanda;
	protected Client client;
	protected Date dataComanda;
	protected Date dataLliurament;   
	protected ComandaEstat estat;	//PENDENT - PREPARAT - TRANSPORT - LLIURAT
	protected Double portes;		//preu de transport
	protected List <ComandaLinia> linies;
	
	public Comanda() {
		idComanda = Generador.getNextComanda();
		dataComanda = new Date();
		dataLliurament = Tools.sumarDies(new Date(), 1);
		estat = ComandaEstat.PENDENT;
		portes = 0.0;
		linies = new ArrayList<ComandaLinia>();
	}

	public Comanda(Client client) {
		this();
		this.client = client;
	}
	
	public Comanda(int idComanda, Client client, Date dataComanda, Date dataLliurament, ComandaEstat estat,
			Double portes, List<ComandaLinia> linies) {
		super();
		this.idComanda = idComanda;
		this.client = client;
		this.dataComanda = dataComanda;
		this.dataLliurament = dataLliurament;
		this.estat = estat;
		this.portes = portes;
		this.linies = linies;
	}

	public List <ComandaLinia> getLinies (){
		return linies;
	}
	
	@Override
	public int compareTo(Comanda c) {
		return (getIdComanda()-(c.getIdComanda()));
	}
	
	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Date getDataComanda() {
		return dataComanda;
	}

	public void setDataComanda(Date dataComanda) {
		this.dataComanda = dataComanda;
	}

	public Date getDataLliurament() {
		return dataLliurament;
	}

	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}

	public ComandaEstat getEstat() {
		return estat;
	}

	public void setEstat(ComandaEstat estat) {
		this.estat = estat;
	}

	public Double getPortes() {
		return portes;
	}

	public void setPortes(Double portes) {
		this.portes = portes;
	}

	public void setLinies(List<ComandaLinia> linies) {
		this.linies = linies;
	}
	
	
}