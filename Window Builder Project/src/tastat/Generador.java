package tastat;

public class Generador{
	private static int lotActual=0;
	private static int producteActual = 0;
	private static int clientActual = 0;
	private static int proveidorActual = 0;
	private static int comandaActual = 0;
	
	public static int getNextLot() {
		return ++lotActual;
	}
	
	public static int getNextProducte() {
		return ++producteActual;
	}
	
	public static int getNextClient() {
		return ++clientActual;
	}

	public static int getNextComanda() {
		return ++comandaActual;
	}
	public static int getNextProveidor() {
		return ++proveidorActual;
	}

	public static int getLotActual() {
		return lotActual;
	}

	public static void setLotActual(int lotActual) {
		Generador.lotActual = lotActual;
	}

	public static int getProducteActual() {
		return producteActual;
	}

	public static void setProducteActual(int producteActual) {
		Generador.producteActual = producteActual;
	}

	public static int getClientActual() {
		return clientActual;
	}

	public static void setClientActual(int clientActual) {
		Generador.clientActual = clientActual;
	}

	public static int getProveidorActual() {
		return proveidorActual;
	}

	public static void setProveidorActual(int proveidorActual) {
		Generador.proveidorActual = proveidorActual;
	}

	public static int getComandaActual() {
		return comandaActual;
	}

	public static void setComandaActual(int comandaActual) {
		Generador.comandaActual = comandaActual;
	}
	
	
	
}
