package dialog;

import javax.swing.JButton;
import javax.swing.JDialog;

import tastat.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JTextPane;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

public class DialogAfegirModLiniesComanda extends JDialog {
	private boolean correcte = false;
	private boolean modificar = true;
	private JComboBox<Integer> comboIdLinia;
	private JComboBox<Integer> comboIdProducte;
	private JComboBox<String> comboNomProd;
	private JSpinner spinnerQuantitat;
	private JButton btnAfegirLinia;
	private JSpinner spinnerPreuVenda;

	public DialogAfegirModLiniesComanda(Magatzem prova, Comanda Comanda) {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");

		setMinimumSize(new Dimension(230, 210));
		setTitle("Linia Comanda");
		setModalityType(ModalityType.APPLICATION_MODAL);
		getContentPane().setLayout(null);

		JTextPane txtIdLinia = new JTextPane();
		txtIdLinia.setBackground(Color.LIGHT_GRAY);
		txtIdLinia.setText("Id Linia:");
		txtIdLinia.setEditable(false);
		txtIdLinia.setBounds(10, 10, 86, 20);
		getContentPane().add(txtIdLinia);

		JTextPane txtIdProducte = new JTextPane();
		txtIdProducte.setText("Id Producte:");
		txtIdProducte.setEditable(false);
		txtIdProducte.setBackground(Color.LIGHT_GRAY);
		txtIdProducte.setBounds(10, 35, 86, 20);
		getContentPane().add(txtIdProducte);

		JTextPane txtNomProd = new JTextPane();
		txtNomProd.setText("Nom Producte:");
		txtNomProd.setEditable(false);
		txtNomProd.setBackground(Color.LIGHT_GRAY);
		txtNomProd.setBounds(10, 60, 86, 20);
		getContentPane().add(txtNomProd);

		JTextPane txtQuantitat = new JTextPane();
		txtQuantitat.setText("Quantitat:");
		txtQuantitat.setEditable(false);
		txtQuantitat.setBackground(Color.LIGHT_GRAY);
		txtQuantitat.setBounds(10, 85, 86, 20);
		getContentPane().add(txtQuantitat);

		JTextPane txtPreuVenda = new JTextPane();
		txtPreuVenda.setText("Preu Venda:");
		txtPreuVenda.setEditable(false);
		txtPreuVenda.setBackground(Color.LIGHT_GRAY);
		txtPreuVenda.setBounds(10, 110, 86, 20);
		getContentPane().add(txtPreuVenda);

		btnAfegirLinia = new JButton("+");
		btnAfegirLinia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int linies = Comanda.getLinies().size();
				btnAfegirLinia.setEnabled(false);
				comboIdLinia.addItem(linies + 1);
				comboIdLinia.setSelectedItem(linies + 1);
				comboIdLinia.setEnabled(false);
				comboIdLinia.setSelectedIndex(0);
				comboNomProd.setSelectedIndex(0);
				spinnerPreuVenda.setValue(0);
				spinnerQuantitat.setValue(0);
				modificar = false;
			}
		});
		btnAfegirLinia.setBounds(149, 10, 43, 20);
		getContentPane().add(btnAfegirLinia);

		comboIdLinia = new JComboBox<Integer>();
		comboIdLinia.setBounds(106, 10, 43, 20);
		if (Comanda.getLinies() != null) {
			int linies = 0;
			for (ComandaLinia cl : Comanda.getLinies()) {
				linies++;
				comboIdLinia.addItem(linies);
			}
		} else {
			comboIdLinia.addItem(1);
			btnAfegirLinia.setEnabled(false);
			modificar = false;
		}
		getContentPane().add(comboIdLinia);

		comboIdProducte = new JComboBox<Integer>();
		comboIdProducte.setBounds(106, 35, 86, 20);
		getContentPane().add(comboIdProducte);

		comboNomProd = new JComboBox<String>();
		comboNomProd.setBounds(106, 60, 86, 20);
		getContentPane().add(comboNomProd);

		comboIdProducte.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Integer idProd = (Integer) comboIdProducte.getSelectedItem();
				String nomprod = null;

				for (Producte p : prova.getProductes()) {
					if (p.getCodiProducte() == idProd) {
						nomprod = p.getNomProducte();
						break;
					}
				}

				if (nomprod != null)
					comboNomProd.setSelectedItem(nomprod);
			}
		});

		comboNomProd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Integer idProd = null;
				String nomprod = (String) comboNomProd.getSelectedItem();

				for (Producte p : prova.getProductes()) {
					if (p.getNomProducte().equals(nomprod)) {
						idProd = p.getCodiProducte();
						break;
					}
				}

				if (idProd != null)
					comboIdProducte.setSelectedItem(idProd);
			}
		});

		comboIdLinia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(comboIdLinia.isEnabled() && btnAfegirLinia.isEnabled()) {
					int idLinia = (int) comboIdLinia.getSelectedItem();
					ComandaLinia linia = Comanda.getLinies().get(idLinia - 1);
					
					comboIdProducte.setSelectedItem(linia.getProducte().getCodiProducte());
					comboNomProd.setSelectedItem(linia.getProducte().getNomProducte());
					spinnerPreuVenda.setValue(linia.getPreuVenda());
					spinnerQuantitat.setValue(linia.getQuantitat());
				}
			}
		});

		for (Producte p : prova.getProductes()) {
			if (p.getTipus() == Tipus.VENDIBLE) {
				comboIdProducte.addItem(p.getCodiProducte());
				comboNomProd.addItem(p.getNomProducte());
			}
		}

		spinnerQuantitat = new JSpinner();
		spinnerQuantitat.setBounds(106, 85, 86, 20);
		getContentPane().add(spinnerQuantitat);

		spinnerPreuVenda = new JSpinner();
		spinnerPreuVenda.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		spinnerPreuVenda.setBounds(106, 110, 86, 20);
		getContentPane().add(spinnerPreuVenda);

		JButton btnAccept = new JButton("ACCEPTAR");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				correcte = true;
				setVisible(false);
			}
		});
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAccept.setBounds(10, 141, 86, 23);
		getContentPane().add(btnAccept);

		JButton btnCancel = new JButton("CANCELAR");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnCancel.setBounds(106, 141, 86, 23);
		getContentPane().add(btnCancel);
		
		if(Comanda.getLinies() != null) {
			ComandaLinia linia = Comanda.getLinies().get(0);
			comboIdProducte.setSelectedItem(linia.getProducte().getCodiProducte());
			comboNomProd.setSelectedItem(linia.getProducte().getNomProducte());
			spinnerPreuVenda.setValue(linia.getPreuVenda());
			spinnerQuantitat.setValue(linia.getQuantitat());
		}
		

	}

	public boolean isCorrecte() {
		return correcte;
	}

	public void setCorrecte(boolean correcte) {
		this.correcte = correcte;
	}

	public JComboBox<Integer> getComboIdLinia() {
		return comboIdLinia;
	}

	public void setComboIdLinia(JComboBox<Integer> comboIdLinia) {
		this.comboIdLinia = comboIdLinia;
	}

	public JComboBox<Integer> getComboIdProducte() {
		return comboIdProducte;
	}

	public void setComboIdProducte(JComboBox<Integer> comboIdProducte) {
		this.comboIdProducte = comboIdProducte;
	}

	public JComboBox<String> getComboNomProd() {
		return comboNomProd;
	}

	public void setComboNomProd(JComboBox<String> comboNomProd) {
		this.comboNomProd = comboNomProd;
	}

	public JSpinner getSpinnerQuantitat() {
		return spinnerQuantitat;
	}

	public void setSpinnerQuantitat(JSpinner spinnerQuantitat) {
		this.spinnerQuantitat = spinnerQuantitat;
	}

	public JButton getBtnAfegirComanda() {
		return btnAfegirLinia;
	}

	public void setBtnAfegirComanda(JButton btnAfegirComanda) {
		this.btnAfegirLinia = btnAfegirComanda;
	}

	public JSpinner getSpinnerPreuVenda() {
		return spinnerPreuVenda;
	}

	public void setSpinnerPreuVenda(JSpinner spinnerPreuVenda) {
		this.spinnerPreuVenda = spinnerPreuVenda;
	}

	public boolean isModificar() {
		return modificar;
	}

	public void setModificar(boolean modificar) {
		this.modificar = modificar;
	}
}
