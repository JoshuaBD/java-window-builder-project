package dialog;

import javax.swing.JButton;
import javax.swing.JDialog;

import tastat.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JTextPane;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JComboBox;

public class DialogAfegirComanda extends JDialog {
	private JTextField textIdComanda;
	private JTextField textDataEnt;
	private JDateChooser dateChooser;
	private Date dataLliurament;
	private boolean correcte = false;
	private JComboBox<String> comboEstat;
	private JButton btnCancel;
	private JComboBox<Integer> comboIdClient;
	private JSpinner spinnerPorts;
	private ArrayList<ComandaLinia> linies;
	private JComboBox<String> comboNomClient;

	public DialogAfegirComanda(Magatzem prova, int idComanda) {
		setResizable(false);
		setMinimumSize(new Dimension(230, 260));
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		setTitle("Nova Comanda");
		setModalityType(ModalityType.APPLICATION_MODAL);
		getContentPane().setLayout(null);
		
		JTextPane txtIdComanda = new JTextPane();
		txtIdComanda.setBackground(Color.LIGHT_GRAY);
		txtIdComanda.setText("Id Comanda:");
		txtIdComanda.setEditable(false);
		txtIdComanda.setBounds(10, 10, 86, 20);
		getContentPane().add(txtIdComanda);
		
		JTextPane txtIdClient = new JTextPane();
		txtIdClient.setText("Id Client:");
		txtIdClient.setEditable(false);
		txtIdClient.setBackground(Color.LIGHT_GRAY);
		txtIdClient.setBounds(10, 35, 86, 20);
		getContentPane().add(txtIdClient);
		
		JTextPane txtNomClient = new JTextPane();
		txtNomClient.setText("Nom Client:");
		txtNomClient.setEditable(false);
		txtNomClient.setBackground(Color.LIGHT_GRAY);
		txtNomClient.setBounds(10, 60, 86, 20);
		getContentPane().add(txtNomClient);
		
		JTextPane txtDataEnt = new JTextPane();
		txtDataEnt.setBackground(Color.LIGHT_GRAY);
		txtDataEnt.setText("Data Entrada:");
		txtDataEnt.setEditable(false);
		txtDataEnt.setBounds(10, 85, 86, 20);
		getContentPane().add(txtDataEnt);
		
		JTextPane txtDataCada = new JTextPane();
		txtDataCada.setBackground(Color.LIGHT_GRAY);
		txtDataCada.setText("Data Lliurament:");
		txtDataCada.setEditable(false);
		txtDataCada.setBounds(10, 110, 86, 20);
		getContentPane().add(txtDataCada);
		
		JTextPane txtPorts = new JTextPane();
		txtPorts.setText("Ports:");
		txtPorts.setEditable(false);
		txtPorts.setBackground(Color.LIGHT_GRAY);
		txtPorts.setBounds(10, 135, 86, 20);
		getContentPane().add(txtPorts);
		
		JTextPane txtEstat = new JTextPane();
		txtEstat.setText("Estat:");
		txtEstat.setEditable(false);
		txtEstat.setBackground(Color.LIGHT_GRAY);
		txtEstat.setBounds(10, 160, 86, 20);
		getContentPane().add(txtEstat);
		
		textIdComanda = new JTextField();
		textIdComanda.setEditable(false);
		textIdComanda.setBounds(106, 10, 86, 20);
		textIdComanda.setText(Integer.toString(idComanda));
		getContentPane().add(textIdComanda);
		textIdComanda.setColumns(10);
		
		comboIdClient = new JComboBox<Integer>();
		comboIdClient.setBounds(106, 35, 86, 20);
		for(Client c: prova.getClients()) {
			comboIdClient.addItem(c.getIdClient());
		}
		getContentPane().add(comboIdClient);
		
		comboNomClient = new JComboBox<String>();
		comboNomClient.setBounds(106, 60, 107, 20);
		for(Client c: prova.getClients()) {
			comboNomClient.addItem(c.getNomClient());
		}
		getContentPane().add(comboNomClient);
		
		comboIdClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Integer idClient = (Integer) comboIdClient.getSelectedItem();
				String nomClient = null;
				
				for(Client c: prova.getClients()) {
					if(c.getIdClient() == idClient) {
						nomClient = c.getNomClient();
						break;
					}
				}
				
				if(nomClient != null)
					comboNomClient.setSelectedItem(nomClient);
			}
		});
		
		comboNomClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Integer idClient = null;
				String nomClient = (String) comboNomClient.getSelectedItem();
				
				for(Client c: prova.getClients()) {
					if(c.getNomClient().equals(nomClient)) {
						idClient = c.getIdClient();
						break;
					}
				}
				
				if(idClient != null)
					comboIdClient.setSelectedItem(idClient);
			}
		});
		
		
		textDataEnt = new JTextField();
		textDataEnt.setEditable(false);
		textDataEnt.setColumns(10);
		textDataEnt.setBounds(106, 85, 86, 20);
		textDataEnt.setText(format.format(new Date()));
		getContentPane().add(textDataEnt);
		
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd-MM-yyyy");
		dateChooser.setBounds(106, 110, 107, 20);
		getContentPane().add(dateChooser);
		
		JButton btnAccept = new JButton("ACCEPTAR");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(dateChooser.getDate() != null) {
					dataLliurament = dateChooser.getDate();
					correcte = true;
					setVisible(false);
				} else {
					correcte = false;
					setVisible(false);
				}
			}
		});
		
		spinnerPorts = new JSpinner();
		spinnerPorts.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		spinnerPorts.setBounds(106, 135, 86, 20);
		getContentPane().add(spinnerPorts);
		
		comboEstat = new JComboBox<String>();
		comboEstat.setBounds(106, 160, 86, 20);
		comboEstat.addItem(ComandaEstat.LLIURADA.toString());
		comboEstat.addItem(ComandaEstat.PENDENT.toString());
		comboEstat.addItem(ComandaEstat.PREPARADA.toString());
		comboEstat.addItem(ComandaEstat.TRANSPORT.toString());
		getContentPane().add(comboEstat);
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAccept.setBounds(10, 191, 86, 23);
		getContentPane().add(btnAccept);
		
		btnCancel = new JButton("CANCELAR");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnCancel.setBounds(106, 191, 86, 23);
		getContentPane().add(btnCancel);
		
	}

	public JTextField getTextIdComanda() {
		return textIdComanda;
	}

	public void setTextIdComanda(JTextField textIdComanda) {
		this.textIdComanda = textIdComanda;
	}

	public JTextField getTextDataEnt() {
		return textDataEnt;
	}

	public void setTextDataEnt(JTextField textDataEnt) {
		this.textDataEnt = textDataEnt;
	}

	public JDateChooser getDateChooser() {
		return dateChooser;
	}

	public void setDateChooser(JDateChooser dateChooser) {
		this.dateChooser = dateChooser;
	}

	public Date getDataLliurament() {
		return dataLliurament;
	}

	public void setDataLliurament(Date dataLliurament) {
		this.dataLliurament = dataLliurament;
	}

	public boolean isCorrecte() {
		return correcte;
	}

	public void setCorrecte(boolean correcte) {
		this.correcte = correcte;
	}

	public JComboBox<String> getComboEstat() {
		return comboEstat;
	}

	public void setComboEstat(JComboBox<String> comboEstat) {
		this.comboEstat = comboEstat;
	}

	public JComboBox<Integer> getComboIdClient() {
		return comboIdClient;
	}

	public void setComboIdClient(JComboBox<Integer> comboIdClient) {
		this.comboIdClient = comboIdClient;
	}

	public JSpinner getSpinnerPorts() {
		return spinnerPorts;
	}

	public void setSpinnerPorts(JSpinner spinnerPorts) {
		this.spinnerPorts = spinnerPorts;
	}

	public ArrayList<ComandaLinia> getLinies() {
		return linies;
	}

	public void setLinies(ArrayList<ComandaLinia> linies) {
		this.linies = linies;
	}
}
