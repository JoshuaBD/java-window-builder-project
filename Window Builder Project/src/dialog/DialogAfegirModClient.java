package dialog;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import tastat.*;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.Color;

public class DialogAfegirModClient extends JDialog {
	private boolean correcte = false;
	private int stockMinim;
	private int preuVenda;
	private JTextField textTelefon;
	private JTextField textContacte;
	private JTextField textPais;
	private JTextField textPoblacio;
	private JTextField textDireccio;
	private JTextField textCIF;
	private JTextField textNomClient;
	private JTextField textIdClient;
	private JSpinner spinnerLongitud, spinnerLatitud;
	
	public DialogAfegirModClient(Magatzem prova, int id) {
		setResizable(false);
		setMinimumSize(new Dimension(230, 330));
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Afegir Producte");
		getContentPane().setLayout(null);
		
		JTextPane txtIdClient = new JTextPane();
		txtIdClient.setText("id Client:");
		txtIdClient.setEditable(false);
		txtIdClient.setBackground(Color.LIGHT_GRAY);
		txtIdClient.setBounds(10, 11, 95, 20);
		getContentPane().add(txtIdClient);
		
		JTextPane txtNomClient = new JTextPane();
		txtNomClient.setText("Nom Client:");
		txtNomClient.setEditable(false);
		txtNomClient.setBackground(Color.LIGHT_GRAY);
		txtNomClient.setBounds(10, 36, 95, 20);
		getContentPane().add(txtNomClient);
		
		JTextPane txtCIF = new JTextPane();
		txtCIF.setText("CIF:");
		txtCIF.setEditable(false);
		txtCIF.setBackground(Color.LIGHT_GRAY);
		txtCIF.setBounds(10, 61, 95, 20);
		getContentPane().add(txtCIF);
		
		JTextPane txtDireccio = new JTextPane();
		txtDireccio.setText("Direcció:");
		txtDireccio.setEditable(false);
		txtDireccio.setBackground(Color.LIGHT_GRAY);
		txtDireccio.setBounds(10, 86, 95, 20);
		getContentPane().add(txtDireccio);
		
		JTextPane txtPoblacio = new JTextPane();
		txtPoblacio.setText("Poblacio:");
		txtPoblacio.setEditable(false);
		txtPoblacio.setBackground(Color.LIGHT_GRAY);
		txtPoblacio.setBounds(10, 111, 95, 20);
		getContentPane().add(txtPoblacio);
		
		JTextPane txtPais = new JTextPane();
		txtPais.setText("Pais:");
		txtPais.setEditable(false);
		txtPais.setBackground(Color.LIGHT_GRAY);
		txtPais.setBounds(10, 136, 95, 20);
		getContentPane().add(txtPais);
		
		JTextPane txtContacte = new JTextPane();
		txtContacte.setText("Contacte:");
		txtContacte.setEditable(false);
		txtContacte.setBackground(Color.LIGHT_GRAY);
		txtContacte.setBounds(10, 161, 95, 20);
		getContentPane().add(txtContacte);
		
		JTextPane txtTelefon = new JTextPane();
		txtTelefon.setText("Telefon:");
		txtTelefon.setEditable(false);
		txtTelefon.setBackground(Color.LIGHT_GRAY);
		txtTelefon.setBounds(10, 186, 95, 20);
		getContentPane().add(txtTelefon);
		
		JTextPane txtLatitud = new JTextPane();
		txtLatitud.setText("Latitud:");
		txtLatitud.setEditable(false);
		txtLatitud.setBackground(Color.LIGHT_GRAY);
		txtLatitud.setBounds(10, 211, 95, 20);
		getContentPane().add(txtLatitud);
		
		JTextPane txtLongitud = new JTextPane();
		txtLongitud.setText("Longitud:");
		txtLongitud.setEditable(false);
		txtLongitud.setBackground(Color.LIGHT_GRAY);
		txtLongitud.setBounds(10, 236, 95, 20);
		getContentPane().add(txtLongitud);
		
		textIdClient = new JTextField();
		textIdClient.setColumns(10);
		textIdClient.setBounds(110, 11, 96, 20);
		textIdClient.setEditable(false);
		textIdClient.setText(Integer.toString(id));
		getContentPane().add(textIdClient);
		
		textNomClient = new JTextField();
		textNomClient.setColumns(10);
		textNomClient.setBounds(110, 36, 96, 20);
		getContentPane().add(textNomClient);
		
		textCIF = new JTextField();
		textCIF.setColumns(10);
		textCIF.setBounds(110, 61, 96, 20);
		getContentPane().add(textCIF);
		
		textDireccio = new JTextField();
		textDireccio.setColumns(10);
		textDireccio.setBounds(110, 86, 96, 20);
		getContentPane().add(textDireccio);
		
		textPoblacio = new JTextField();
		textPoblacio.setColumns(10);
		textPoblacio.setBounds(110, 111, 96, 20);
		getContentPane().add(textPoblacio);
		
		textPais = new JTextField();
		textPais.setColumns(10);
		textPais.setBounds(110, 136, 96, 20);
		getContentPane().add(textPais);
		
		textContacte = new JTextField();
		textContacte.setColumns(10);
		textContacte.setBounds(110, 161, 96, 20);
		getContentPane().add(textContacte);
		
		textTelefon = new JTextField();
		textTelefon.setBounds(110, 186, 96, 20);
		getContentPane().add(textTelefon);
		textTelefon.setColumns(10);
		
		spinnerLatitud = new JSpinner();
		spinnerLatitud.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1.0)));
		spinnerLatitud.setBounds(110, 211, 96, 20);
		getContentPane().add(spinnerLatitud);
		
		spinnerLongitud = new JSpinner();
		spinnerLongitud.setModel(new SpinnerNumberModel(new Double(0), null, null, new Double(1.0)));
		spinnerLongitud.setBounds(110, 236, 96, 20);
		getContentPane().add(spinnerLongitud);
		
		JButton btnAccept = new JButton("ACCEPTAR");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!textNomClient.getText().equals("")) {
					correcte = true;
					setVisible(false);
				} else {
					correcte = false;
					setVisible(false);
				}

			}
		});
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAccept.setBounds(20, 267, 86, 23);
		getContentPane().add(btnAccept);

		JButton btnCancel = new JButton("CANCELAR");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnCancel.setBounds(120, 267, 86, 23);
		getContentPane().add(btnCancel);
		
		/* AQUI ES DECIDEIX SI ES MODIFICA UN PRODUCTE EXISTENT O S'AFEGEIX UN PRODUCTE NOU */
		
		Client client = null;
		for (Client c : prova.getClients()) {
			if (c.getIdClient() == id) {
				client = c;
				break;
			}
		}
		
		if(client != null) {
			textIdClient.setText(Integer.toString(client.getIdClient()));
			textNomClient.setText(client.getNomClient());
			textCIF.setText(client.getCIF());
			textDireccio.setText(client.getDireccio());
			textPoblacio.setText(client.getPoblacio());
			textPais.setText(client.getPais());
			textContacte.setText(client.getPersonaContacte());
			textTelefon.setText(client.getTelefon());
			spinnerLatitud.setValue(client.getLatitud());
			spinnerLongitud.setValue(client.getLongitud());
		}
	}

	public boolean isCorrecte() {
		return correcte;
	}

	public void setCorrecte(boolean correcte) {
		this.correcte = correcte;
	}

	public int getStockMinim() {
		return stockMinim;
	}

	public void setStockMinim(int stockMinim) {
		this.stockMinim = stockMinim;
	}

	public int getPreuVenda() {
		return preuVenda;
	}

	public void setPreuVenda(int preuVenda) {
		this.preuVenda = preuVenda;
	}

	public JTextField getTextTelefon() {
		return textTelefon;
	}

	public void setTextTelefon(JTextField textTelefon) {
		this.textTelefon = textTelefon;
	}

	public JTextField getTextContacte() {
		return textContacte;
	}

	public void setTextContacte(JTextField textContacte) {
		this.textContacte = textContacte;
	}

	public JTextField getTextPais() {
		return textPais;
	}

	public void setTextPais(JTextField textPais) {
		this.textPais = textPais;
	}

	public JTextField getTextPoblacio() {
		return textPoblacio;
	}

	public void setTextPoblacio(JTextField textPoblacio) {
		this.textPoblacio = textPoblacio;
	}

	public JTextField getTextDireccio() {
		return textDireccio;
	}

	public void setTextDireccio(JTextField textDireccio) {
		this.textDireccio = textDireccio;
	}

	public JTextField getTextCIF() {
		return textCIF;
	}

	public void setTextCIF(JTextField textCIF) {
		this.textCIF = textCIF;
	}

	public JTextField getTextNomClient() {
		return textNomClient;
	}

	public void setTextNomClient(JTextField textNomClient) {
		this.textNomClient = textNomClient;
	}

	public JTextField getTextIdClient() {
		return textIdClient;
	}

	public void setTextIdClient(JTextField textIdClient) {
		this.textIdClient = textIdClient;
	}

	public JSpinner getSpinnerLongitud() {
		return spinnerLongitud;
	}

	public void setSpinnerLongitud(JSpinner spinnerLongitud) {
		this.spinnerLongitud = spinnerLongitud;
	}

	public JSpinner getSpinnerLatitud() {
		return spinnerLatitud;
	}

	public void setSpinnerLatitud(JSpinner spinnerLatitud) {
		this.spinnerLatitud = spinnerLatitud;
	}
}
