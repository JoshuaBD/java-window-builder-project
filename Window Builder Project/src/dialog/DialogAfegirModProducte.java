package dialog;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import tastat.*;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.Color;

public class DialogAfegirModProducte extends JDialog {
	private boolean correcte = false;
	private JTextField textIdProd;
	private JTextField textNom;
	private JTextField textStock;
	private JSpinner spinnerStockMinim;
	private JSpinner spinnerPreuVenda;
	private int stockMinim;
	private int preuVenda;
	JComboBox<String> comboTipusProd, comboUnitatMesura, comboProveidor;

	public DialogAfegirModProducte(Magatzem prova, int id) {
		setResizable(false);
		setMinimumSize(new Dimension(270, 290));
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		setTitle("Afegir o Modificar Producte");
		getContentPane().setLayout(null);

		JTextPane txtpnIdproducte = new JTextPane();
		txtpnIdproducte.setText("idProducte:");
		txtpnIdproducte.setEditable(false);
		txtpnIdproducte.setBackground(Color.LIGHT_GRAY);
		txtpnIdproducte.setBounds(10, 11, 95, 20);
		getContentPane().add(txtpnIdproducte);

		JTextPane txtpnNom = new JTextPane();
		txtpnNom.setText("Nom:");
		txtpnNom.setEditable(false);
		txtpnNom.setBackground(Color.LIGHT_GRAY);
		txtpnNom.setBounds(10, 36, 95, 20);
		getContentPane().add(txtpnNom);

		JTextPane txtpnPreuVenda = new JTextPane();
		txtpnPreuVenda.setText("Preu Venda:");
		txtpnPreuVenda.setEditable(false);
		txtpnPreuVenda.setBackground(Color.LIGHT_GRAY);
		txtpnPreuVenda.setBounds(10, 61, 95, 20);
		getContentPane().add(txtpnPreuVenda);

		JTextPane txtpnStock = new JTextPane();
		txtpnStock.setText("Stock:");
		txtpnStock.setEditable(false);
		txtpnStock.setBackground(Color.LIGHT_GRAY);
		txtpnStock.setBounds(10, 86, 95, 20);
		getContentPane().add(txtpnStock);

		JTextPane txtpnStockMnim = new JTextPane();
		txtpnStockMnim.setText("Stock Mínim:");
		txtpnStockMnim.setEditable(false);
		txtpnStockMnim.setBackground(Color.LIGHT_GRAY);
		txtpnStockMnim.setBounds(10, 111, 95, 20);
		getContentPane().add(txtpnStockMnim);

		JTextPane txtpnTipus = new JTextPane();
		txtpnTipus.setText("Tipus Producte:");
		txtpnTipus.setEditable(false);
		txtpnTipus.setBackground(Color.LIGHT_GRAY);
		txtpnTipus.setBounds(10, 136, 95, 20);
		getContentPane().add(txtpnTipus);

		JTextPane txtpnUnitat = new JTextPane();
		txtpnUnitat.setText("Unitat Mesura:");
		txtpnUnitat.setEditable(false);
		txtpnUnitat.setBackground(Color.LIGHT_GRAY);
		txtpnUnitat.setBounds(10, 161, 95, 20);
		getContentPane().add(txtpnUnitat);
		
		JTextPane textPane = new JTextPane();
		textPane.setText("Proveidor:");
		textPane.setEditable(false);
		textPane.setBackground(Color.LIGHT_GRAY);
		textPane.setBounds(10, 186, 95, 20);
		getContentPane().add(textPane);
		
		textIdProd = new JTextField();
		textIdProd.setHorizontalAlignment(SwingConstants.RIGHT);
		textIdProd.setEditable(false);
		textIdProd.setColumns(10);
		textIdProd.setBounds(110, 11, 95, 20);
		textIdProd.setText(Integer.toString(id));
		getContentPane().add(textIdProd);

		textNom = new JTextField();
		textNom.setColumns(10);
		textNom.setBounds(110, 36, 132, 20);
		getContentPane().add(textNom);

		spinnerPreuVenda = new JSpinner();
		spinnerPreuVenda.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		spinnerPreuVenda.setBounds(110, 61, 95, 20);
		getContentPane().add(spinnerPreuVenda);

		textStock = new JTextField();
		textStock.setHorizontalAlignment(SwingConstants.RIGHT);
		textStock.setText("0");
		textStock.setEditable(false);
		textStock.setColumns(10);
		textStock.setBounds(110, 86, 95, 20);
		getContentPane().add(textStock);

		spinnerStockMinim = new JSpinner();
		spinnerStockMinim.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinnerStockMinim.setBounds(110, 111, 95, 20);
		getContentPane().add(spinnerStockMinim);

		comboTipusProd = new JComboBox<String>();
		comboTipusProd.setBounds(110, 136, 120, 20);
		comboTipusProd.addItem("INGREDIENT");
		comboTipusProd.addItem("VENDIBLE");
		getContentPane().add(comboTipusProd);
		
		comboUnitatMesura = new JComboBox<String>();
		comboUnitatMesura.setBounds(110, 161, 120, 20);
		comboUnitatMesura.addItem("GRAMS");
		comboUnitatMesura.addItem("LLITRE");
		comboUnitatMesura.addItem("UNITAT");
		getContentPane().add(comboUnitatMesura);
		
		comboProveidor = new JComboBox<String>();
		comboProveidor.setBounds(110, 186, 120, 20);
		for(Proveidor p: prova.getProveidors()) {
			comboProveidor.addItem(p.getNomProveidor());
		}
		comboProveidor.addItem("No té proveidor");
		getContentPane().add(comboProveidor);
		
		/* AQUI ES DECIDEIX SI ES MODIFICA UN PRODUCTE EXISTENT O S'AFEGEIX UN PRODUCTE NOU */
		
		Producte prod = null;
		for (Producte p : prova.getProductes()) {
			if (p.getCodiProducte() == id) {
				prod = p;
				break;
			}
		}
		
		if(prod != null) {
			textNom.setText(prod.getNomProducte());
			spinnerPreuVenda.setValue(prod.getPreuVenda());
			spinnerStockMinim.setValue(prod.getStockMinim());
			textStock.setText(Integer.toString(prod.getStock()));
			comboTipusProd.setSelectedItem(prod.getTipus().toString());
			comboUnitatMesura.setSelectedItem(prod.getUnitatMesura().toString());
			if(prod.getProveidor() == null)
				comboProveidor.setSelectedItem("No té proveidor");
			else
				comboProveidor.setSelectedItem(prod.getProveidor().toString());
		}
		
		
		

		JButton btnAccept = new JButton("ACCEPTAR");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!textNom.getText().equals("")) {
					correcte = true;
					setVisible(false);
				} else {
					correcte = false;
					setVisible(false);
				}

			}
		});
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAccept.setBounds(19, 220, 86, 23);
		getContentPane().add(btnAccept);

		JButton btnCancel = new JButton("CANCELAR");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnCancel.setBounds(119, 220, 86, 23);
		getContentPane().add(btnCancel);

	}

	public JTextField getTextNom() {
		return textNom;
	}

	public void setTextNom(JTextField textNom) {
		this.textNom = textNom;
	}

	public JTextField getTextStock() {
		return textStock;
	}

	public void setTextStock(JTextField textStock) {
		this.textStock = textStock;
	}

	public int getStockMinim() {
		return stockMinim;
	}

	public void setStockMinim(int stockMinim) {
		this.stockMinim = stockMinim;
	}

	public int getPreuVenda() {
		return preuVenda;
	}

	public void setPreuVenda(int preuVenda) {
		this.preuVenda = preuVenda;
	}

	public boolean isCorrecte() {
		return correcte;
	}

	public JSpinner getSpinnerStockMinim() {
		return spinnerStockMinim;
	}

	public void setSpinnerStockMinim(JSpinner spinnerStockMinim) {
		this.spinnerStockMinim = spinnerStockMinim;
	}

	public JSpinner getSpinnerPreuVenda() {
		return spinnerPreuVenda;
	}

	public void setSpinnerPreuVenda(JSpinner spinnerPreuVenda) {
		this.spinnerPreuVenda = spinnerPreuVenda;
	}

	public JComboBox<String> getComboTipusProd() {
		return comboTipusProd;
	}

	public void setComboTipusProd(JComboBox<String> comboTipusProd) {
		this.comboTipusProd = comboTipusProd;
	}

	public JComboBox<String> getComboUnitatMesura() {
		return comboUnitatMesura;
	}

	public void setComboUnitatMesura(JComboBox<String> comboUnitatMesura) {
		this.comboUnitatMesura = comboUnitatMesura;
	}

	public JComboBox<String> getComboProveidor() {
		return comboProveidor;
	}

	public void setComboProveidor(JComboBox<String> comboProveidor) {
		this.comboProveidor = comboProveidor;
	}
}
