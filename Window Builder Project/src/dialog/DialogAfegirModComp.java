package dialog;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import tastat.Magatzem;
import tastat.Producte;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Window;
import java.awt.Dialog.ModalityType;
import java.awt.Color;

public class DialogAfegirModComp extends JDialog {
	private JTextPane txtID;
	private JTextPane txtNomProd;
	private JTextPane txtQuantitat;
	private JComboBox<Integer> comboID;
	private JComboBox<String> comboNom;
	private JSpinner spinQuantitat;
	private boolean correcte = false;
	
	public DialogAfegirModComp(Magatzem prova, int id) {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		setTitle("Afegir Component");
		setMinimumSize(new Dimension(210, 155));
		setResizable(false);
		getContentPane().setLayout(null);
		
		txtID = new JTextPane();
		txtID.setBackground(Color.LIGHT_GRAY);
		txtID.setEditable(false);
		txtID.setText("idProducte:");
		txtID.setBounds(10, 10, 86, 20);
		getContentPane().add(txtID);
		
		txtNomProd = new JTextPane();
		txtNomProd.setBackground(Color.LIGHT_GRAY);
		txtNomProd.setEditable(false);
		txtNomProd.setText("Nom Producte:");
		txtNomProd.setBounds(10, 35, 86, 20);
		getContentPane().add(txtNomProd);
		
		txtQuantitat = new JTextPane();
		txtQuantitat.setBackground(Color.LIGHT_GRAY);
		txtQuantitat.setEditable(false);
		txtQuantitat.setText("Quantitat:");
		txtQuantitat.setBounds(10, 60, 86, 20);
		getContentPane().add(txtQuantitat);
		
		comboID = new JComboBox<Integer>();
		
		for(Producte p: prova.getProductes()) {
			if(p.getCodiProducte() != id)
				comboID.addItem(p.getCodiProducte());
		}
		
		comboID.setBounds(106, 10, 48, 20);
		getContentPane().add(comboID);
		
		comboNom = new JComboBox<String>();
		
		for(Producte p: prova.getProductes()) {
			if(p.getCodiProducte() != id)
				comboNom.addItem(p.getNomProducte());
		}
		
		comboNom.setBounds(106, 35, 86, 20);
		getContentPane().add(comboNom);
		
		comboID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Integer idProd = (Integer) comboID.getSelectedItem();
				String nomprod = null;
				
				for(Producte p: prova.getProductes()) {
					if(p.getCodiProducte() == idProd) {
						nomprod = p.getNomProducte();
						break;
					}
				}
				
				if(nomprod != null)
					comboNom.setSelectedItem(nomprod);
			}
		});
		
		comboNom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Integer idProd = null;
				String nomprod = (String) comboNom.getSelectedItem();
				
				for(Producte p: prova.getProductes()) {
					if(p.getNomProducte().equals(nomprod)) {
						idProd = p.getCodiProducte();
						break;
					}
				}
				
				if(idProd != null)
					comboID.setSelectedItem(idProd);
			}
		});
		
		spinQuantitat = new JSpinner();
		spinQuantitat.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinQuantitat.setBounds(106, 60, 86, 20);
		getContentPane().add(spinQuantitat);
		
		JButton btnAccept = new JButton("ACCEPTAR");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				correcte = true;
				setVisible(false);
				
			}
		});
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAccept.setBounds(10, 91, 86, 23);
		getContentPane().add(btnAccept);
		
		JButton btnCancel = new JButton("CANCELAR");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnCancel.setBounds(106, 91, 86, 23);
		getContentPane().add(btnCancel);
		
	}

	public JComboBox<Integer> getComboID() {
		return comboID;
	}

	public void setComboID(JComboBox<Integer> comboID) {
		this.comboID = comboID;
	}

	public JComboBox<String> getComboNom() {
		return comboNom;
	}

	public void setComboNom(JComboBox<String> comboNom) {
		this.comboNom = comboNom;
	}

	public JSpinner getSpinQuantitat() {
		return spinQuantitat;
	}

	public void setSpinQuantitat(JSpinner spinQuantitat) {
		this.spinQuantitat = spinQuantitat;
	}
	
	public boolean getCorrecte() {
		return correcte;
	}
	
	
}
