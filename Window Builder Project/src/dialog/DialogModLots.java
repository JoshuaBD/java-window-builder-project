package dialog;

import javax.swing.JButton;
import javax.swing.JDialog;

import tastat.LotDesglossat;
import tastat.Magatzem;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTextPane;
import javax.swing.JTextField;
import com.toedter.calendar.JDateChooser;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class DialogModLots extends JDialog {
	private JTextField textIdLot;
	private JDateChooser dateChooserCad;
	private JDateChooser dateChooserEntr;
	private Date dataCad;
	private Date dataEnt;
	private JSpinner spinnerQuant;
	private boolean correcte = false;

	public DialogModLots(Magatzem prova, int idProd, LotDesglossat lot) {
		getContentPane().setBackground(Color.LIGHT_GRAY);
		setResizable(false);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		
		setMinimumSize(new Dimension(230, 175));
		setTitle("Afegir Lot");
		setModalityType(ModalityType.APPLICATION_MODAL);
		getContentPane().setLayout(null);
		
		JTextPane txtIdLot = new JTextPane();
		txtIdLot.setBackground(Color.LIGHT_GRAY);
		txtIdLot.setText("id Lot:");
		txtIdLot.setEditable(false);
		txtIdLot.setBounds(10, 10, 86, 20);
		getContentPane().add(txtIdLot);
		
		JTextPane txtDataEnt = new JTextPane();
		txtDataEnt.setBackground(Color.LIGHT_GRAY);
		txtDataEnt.setText("Data Entrada:");
		txtDataEnt.setEditable(false);
		txtDataEnt.setBounds(10, 35, 86, 20);
		getContentPane().add(txtDataEnt);
		
		JTextPane txtDataCada = new JTextPane();
		txtDataCada.setBackground(Color.LIGHT_GRAY);
		txtDataCada.setText("Data Caduc:");
		txtDataCada.setEditable(false);
		txtDataCada.setBounds(10, 60, 86, 20);
		getContentPane().add(txtDataCada);
		
		JTextPane txtQuant = new JTextPane();
		txtQuant.setBackground(Color.LIGHT_GRAY);
		txtQuant.setText("Quantitat:");
		txtQuant.setEditable(false);
		txtQuant.setBounds(10, 85, 86, 20);
		getContentPane().add(txtQuant);
		
		textIdLot = new JTextField();
		textIdLot.setEditable(false);
		textIdLot.setBounds(106, 10, 86, 20);
		textIdLot.setText(Integer.toString(lot.getLot()));
		getContentPane().add(textIdLot);
		textIdLot.setColumns(10);
		
		dateChooserEntr = new JDateChooser();
		dateChooserEntr.setDateFormatString("dd-MM-yyyy");
		dateChooserEntr.setDate(lot.getDataEntrada());
		dateChooserEntr.setBounds(106, 35, 107, 20);
		getContentPane().add(dateChooserEntr);
		
		dateChooserCad = new JDateChooser();
		dateChooserCad.setDateFormatString("dd-MM-yyyy");
		dateChooserCad.setDate(lot.getDataCaducitat());
		dateChooserCad.setBounds(106, 60, 107, 20);
		getContentPane().add(dateChooserCad);
		
		spinnerQuant = new JSpinner();
		spinnerQuant.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		spinnerQuant.setValue(lot.getQuantitat());
		spinnerQuant.setBounds(106, 85, 86, 20);
		getContentPane().add(spinnerQuant);
		
		JButton btnAccept = new JButton("ACCEPTAR");
		btnAccept.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(dateChooserCad.getDate() != null && dateChooserEntr.getDate() != null) {
					dataCad = dateChooserCad.getDate();
					dataEnt = dateChooserEntr.getDate();
					correcte = true;
					setVisible(false);
				} else {
					correcte = false;
					setVisible(false);
				}
			}
		});
		btnAccept.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAccept.setBounds(10, 116, 86, 23);
		getContentPane().add(btnAccept);
		
		JButton btnCancel = new JButton("CANCELAR");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		btnCancel.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnCancel.setBounds(106, 116, 86, 23);
		getContentPane().add(btnCancel);
		
	}

	public JTextField getTextIdLot() {
		return textIdLot;
	}

	public void setTextIdLot(JTextField textIdLot) {
		this.textIdLot = textIdLot;
	}

	public JSpinner getSpinnerQuant() {
		return spinnerQuant;
	}

	public void setSpinnerQuant(JSpinner spinnerQuant) {
		this.spinnerQuant = spinnerQuant;
	}

	public Date getDataEnt() {
		return dataEnt;
	}

	public void setDataEnt(Date dataEnt) {
		this.dataEnt = dataEnt;
	}

	public boolean isCorrecte() {
		return correcte;
	}

	public void setCorrecte(boolean correcte) {
		this.correcte = correcte;
	}

	public Date getDataCad() {
		return dataCad;
	}

	public void setDataCad(Date dataCad) {
		this.dataCad = dataCad;
	}
}
